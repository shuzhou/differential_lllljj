#/bin/bash
file="mc16_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l_SR.root"
dir="BDT-truth-0517"
BDT="BDT"
tfile="tree_name2.txt"
cp ClassApplication.C.bak ClassApplication.C
sed -i "s/MYDIR/$dir/g" ClassApplication.C
sed -i "s/MYFILE/$file/g" ClassApplication.C
sed -i "s/MYBDTN/$BDT/g" ClassApplication.C
sed -i "s/MYTNAME/$tfile/g" ClassApplication.C
root -l ClassApplication.C
