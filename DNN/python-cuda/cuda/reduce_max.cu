#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "reduce_max.cuh"
__global__ void gpu_max_func(float *input_vec, float *output_vec, long long size){
	extern __shared__ float sharedMem[];
	int stride = blockDim.x*gridDim.x;
	int index = blockIdx.x*blockDim.x+threadIdx.x;
	int tid = threadIdx.x;
	sharedMem[tid] = -99.0;
   	 __syncthreads();
	for(int i=index; i<size;i=i+stride){
		if(sharedMem[tid]<input_vec[i]){
			sharedMem[tid] = input_vec[i];
		}
	}
	__syncthreads();
	for(int offset = blockDim.x/2; offset>0; offset>>=1){
		if(tid<offset){
			if(sharedMem[tid+offset]>sharedMem[tid]){
				sharedMem[tid]=sharedMem[tid+offset];
			}
                 
		}
		 __syncthreads();
	}
	if(threadIdx.x==0){
		output_vec[blockIdx.x] = sharedMem[0];
	}
}
float reduce_max(float *input_vec, long long size){
	float *a;
	float *gpu_max_vec;
	//float cpu_max = -99.0;
	float gpu_max = -99.0;
	const int num_threads = 1024;
	int num_blocks;
	int deviceId;
	int numberOfSMs;
	cudaGetDevice(&deviceId);
	cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
	num_blocks=numberOfSMs;
	dim3 block_num(num_blocks,1,1);
	dim3 threads_per_block(num_threads,1,1);
	//printf("Device ID: %d\tNumber of SMs: %d\n", deviceId, numberOfSMs);
	cudaMallocManaged(&a, size*sizeof(float));
	cudaMemcpy(a, input_vec, size*sizeof(float),cudaMemcpyHostToDevice);
	cudaMallocManaged(&gpu_max_vec, num_blocks*sizeof(float));
	cudaMemPrefetchAsync(gpu_max_vec, num_blocks*sizeof(float), deviceId);
	gpu_max_func<<<block_num,threads_per_block,num_threads*sizeof(float)>>> (a, gpu_max_vec, size);
	cudaDeviceSynchronize();
	cudaMemPrefetchAsync(gpu_max_vec, sizeof(gpu_max_vec), cudaCpuDeviceId);
	for(int i=0;i<num_blocks;i++){
		if(gpu_max_vec[i]>gpu_max){
			gpu_max = gpu_max_vec[i];
		}
	}
        return(gpu_max);






}

