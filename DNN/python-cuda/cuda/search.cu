#include <stdio.h>             
#include <math.h>              
#include <cstdlib>
#include "search.cuh"
__global__ void gpu_simple_search(float *input_vec, float *output_vec, float num, int size){
	int x=threadIdx.x;
	int index=blockDim.x*blockIdx.x+x;
	int stride=blockDim.x*gridDim.x;
	if(x==0){
		output_vec[blockIdx.x]=0;
	}
	__syncthreads();
	for(int i=index;i<size;i=i+stride){
		if(input_vec[i]==num){
			atomicAdd(&output_vec[blockIdx.x],1);
		}

	}
}

int search(float *input_vec1, float *input_vec2, int size1, int size2){
	float *a;
	int num_threads=1024;
	int deviceId;
	int numberOfSMs;
	float num;
	int count=0;
	cudaGetDevice(&deviceId);
	cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
	dim3 block_shape(num_threads,1,1);
	dim3 grid_shape(numberOfSMs,1,1);
	cudaMallocManaged(&a,size1*sizeof(float));
	cudaMemcpy(a,input_vec1,size1*sizeof(float),cudaMemcpyHostToDevice);
	for(int i=0;i<size2;i++){
		float *b;
		int temp=0;
		num=input_vec2[i];
		cudaMalloc(&b,numberOfSMs*sizeof(float));
		cudaMemPrefetchAsync(b, numberOfSMs*sizeof(float),deviceId);
		gpu_simple_search<<<grid_shape,block_shape>>>(a,b,num,size1);
		cudaDeviceSynchronize();
		cudaMemPrefetchAsync(b,numberOfSMs*sizeof(float),cudaCpuDeviceId);
		for(int j=0;j<numberOfSMs;j++){
			if(b[j]!=0){
				temp=1;
			}
		}
		count=count+temp;

	}
	return(count);
}
