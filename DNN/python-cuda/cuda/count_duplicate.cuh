#ifndef COUNT_DUPLICATE       
#define COUNT_DUPLICATE       

#include <stdio.h>

extern "C" 

int count_duplicate(float* input_vec, long long size);
    
#endif
