#ifndef REDUCE_ADD       
#define REDUCE_ADD       

#include <stdio.h>

extern "C" 

double reduce_add(float* input_vec, float* weight_vec, long long size);
    
#endif
