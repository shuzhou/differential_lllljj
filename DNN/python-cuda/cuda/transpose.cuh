#ifndef TRANSPOSE       
#define TRANSPOSE  

#include <stdio.h>

extern "C" 

void transpose(float *input_vec, float *output_vec, int n_col, int n_row);

    
#endif
