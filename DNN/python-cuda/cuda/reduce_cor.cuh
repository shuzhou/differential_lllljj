#ifndef REDUCE_COR       
#define REDUCE_COR       

#include <stdio.h>

extern "C" 

double reduce_cor(float* input_vec_a, float *input_vec_b, float* weight_vec, long long size, double mean_a, double mean_b);
    
#endif
