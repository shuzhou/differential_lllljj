#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "sort.cuh"
//
__global__  void gpu_odd_even_sort(float *input_vec, float *output_vec, long long size){
	extern __shared__ float sharedMem[];
	__shared__ int flag;
	float temp;
	int tid = threadIdx.x;
	int index = blockIdx.x*blockDim.x+threadIdx.x;
	if(2*index<size){
		sharedMem[2*tid] = input_vec[2*index];
	}
	else{
		sharedMem[2*tid] = 99.0;
	}
	if((2*index+1)<size){
		sharedMem[2*tid+1] = input_vec[2*index+1];
	}
	else{
		sharedMem[2*tid+1] = 99.0;
	}
	if(tid==0){
		flag=1;
	}
	__syncthreads();
				        
	while(flag > 0){
		if(tid==0){
			flag=0;
		}
		__syncthreads();
		if(sharedMem[2*tid]>sharedMem[2*tid+1]){
			temp = sharedMem[2*tid];
			sharedMem[2*tid] = sharedMem[2*tid+1];
			sharedMem[2*tid+1 ] = temp;
			flag=1;
		}
		__syncthreads();
		if((tid+1)<blockDim.x){
			if(sharedMem[2*tid+1]>sharedMem[2*tid+2]){
				temp = sharedMem[2*tid+1];
				sharedMem[2*tid+1] = sharedMem[2*tid+2];
				sharedMem[2*tid+2] = temp;
				flag=1;
			}
		}
		 __syncthreads();
	}
	if(2*index<size){
		output_vec[2*index] = sharedMem[2*tid];

	}
	if((2*index+1)<size){
		output_vec[2*index+1] = sharedMem[2*tid+1];
	
	}
						                                
						        

	
}


									                        

__global__ void gpu_merge_sort_dis(float *input_vec, float *output_vec, int step_size, int num_bl, long long size){
	int bl_size = step_size/num_bl;
	int num_step = blockIdx.x/num_bl;
	int bid = blockIdx.x;
	int tid = threadIdx.x;
	int section_num = bid%num_bl;
	int left, right, mid, left_loc, right_loc, mid_loc;
	int index_temp, loc_temp;
	for(int i=tid; i<bl_size; i=i+blockDim.x){
		index_temp=2*num_step*step_size+bl_size*section_num+i;
		if(index_temp<size){
			loc_temp = index_temp;
			left = 0;
			right = step_size-1;
			if(((2*num_step+1)*step_size+left)>=size){
				output_vec[loc_temp] = input_vec[index_temp];
				continue;
			}
			if(((2*num_step+1)*step_size+right)>=size){
				right = size-(2*num_step+1)*step_size-1;
			}
			while((right-left)>1){
				mid=(right+left)/2;
				mid_loc=(2*num_step+1)*step_size+mid;
				if(input_vec[index_temp]>input_vec[mid_loc]){
					left = mid;
				}
				else{
					right=mid;
				}
			}
			right_loc = right+(2*num_step+1)*step_size;
			left_loc = left+(2*num_step+1)*step_size;
			if(input_vec[index_temp]<=input_vec[left_loc]){
				loc_temp = index_temp+left;
			}
			else if(input_vec[index_temp]>input_vec[right_loc]){
				loc_temp = index_temp+right+1;
			}
			else{
				loc_temp = index_temp+left+1;
			}
			output_vec[loc_temp] = input_vec[index_temp];
		}

	}
	if((2*num_step+1)*step_size<size){
		for(int i=tid;i<bl_size;i=i+blockDim.x){
			index_temp = (2*num_step+1)*step_size+bl_size*section_num+i;
			if(index_temp<size){
				loc_temp=index_temp;
				left =0;
				right = step_size-1;
				while((right-left)>1){
					mid=(right+left)/2;
					mid_loc = 2*num_step*step_size+mid;
					if(input_vec[index_temp]>=input_vec[mid_loc]){
						left=mid;
					}
					else{
						right =mid;
					}
				}
				right_loc=right+2*num_step*step_size;
				left_loc=left+2*num_step*step_size;
				if(input_vec[index_temp]<input_vec[left_loc]){
					loc_temp=2*num_step*step_size+left+i+bl_size*section_num;
				}
				else if(input_vec[index_temp]>=input_vec[right_loc]){
					loc_temp=2*num_step*step_size+i+bl_size*section_num+right+1;
				}
				else{
					loc_temp=2*num_step*step_size+i+bl_size*section_num+left+1;
				}
				output_vec[loc_temp] = input_vec[index_temp];

			}
		}
	}

}



void sort(float *input_vec, float *output_vec, int *index_count, long long size){
	float *a;
	float *out;
	const int num_threads = 512;
	int step_size = 1024;
	int num_blocks =(int)(((double)size)/(2.0*((double)num_threads)))+1;
	int deviceId;
	int numberOfSMs;
	dim3 block_num(num_blocks,1,1);
	dim3 threads_per_block(num_threads,1,1);
	int numblocks;
	int count;
	cudaError_t addVectorsErr;
	cudaError_t asyncErr;
	cudaGetDevice(&deviceId);
	cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
	cudaMallocManaged(&a, size*sizeof(float));
	cudaMemcpy(a, input_vec, size*sizeof(float),cudaMemcpyHostToDevice);
	cudaMallocManaged(&out, size*sizeof(float));
	cudaMemPrefetchAsync(out, size*sizeof(float), deviceId);
	gpu_odd_even_sort<<<block_num,threads_per_block, 2*num_threads*sizeof(float)>>> (a, out, size);
	cudaDeviceSynchronize();
	count=1;
	for(double i =(double)size/((double)step_size*2.0);i>0.5;i=i/2.0){
		float *temp_out;
		cudaMallocManaged(&temp_out, size*sizeof(float));
		cudaMemPrefetchAsync(temp_out, size*sizeof(float),deviceId);
		numblocks = size/(step_size*2);
		if(numblocks<(double)size/((double)step_size*2.0)){
			numblocks=numblocks+1;
		}
		if(numblocks<128){
			count=count*2;
	        }
		dim3 block_num1(numblocks*count,1,1);
		gpu_merge_sort_dis<<<block_num1,threads_per_block>>>(out, temp_out,step_size,count,size);
		addVectorsErr = cudaGetLastError();
		if(addVectorsErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(addVectorsErr));
		asyncErr = cudaDeviceSynchronize();
        	if(asyncErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(asyncErr));
		cudaDeviceSynchronize();
		cudaMemcpy(out, temp_out, size*sizeof(float),cudaMemcpyDeviceToDevice);
		cudaFree(temp_out);
		printf("Num blocks used is: %d, count is: %d, total block is: %d, step size is: %d\n",numblocks,count, numblocks*count,step_size);
		step_size = step_size*2;
	}
	cudaMemcpy(output_vec, out, size*sizeof(float),cudaMemcpyDeviceToHost);
}

