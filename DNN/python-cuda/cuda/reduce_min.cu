#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "reduce_min.cuh"
__global__ void gpu_min_func(float *input_vec, float *output_vec, long long size){
	extern __shared__ float sharedMem[];
	int stride = blockDim.x*gridDim.x;
	int index = blockIdx.x*blockDim.x+threadIdx.x;
	int tid = threadIdx.x;
	sharedMem[tid] = 99.0;
   	 __syncthreads();
	for(int i=index; i<size;i=i+stride){
		if(sharedMem[tid]>input_vec[i]){
			sharedMem[tid] = input_vec[i];
		}
	}
	__syncthreads();
	for(int offset = blockDim.x/2; offset>0; offset>>=1){
		if(tid<offset){
			if(sharedMem[tid+offset]<sharedMem[tid]){
				sharedMem[tid]=sharedMem[tid+offset];
			}
                 
		}
		 __syncthreads();
	}
	if(threadIdx.x==0){
		output_vec[blockIdx.x] = sharedMem[0];
	}
}
float reduce_min(float *input_vec, long long size){
	float *a;
	float *gpu_min_vec;
	//float cpu_min = 99.0;
	float gpu_min = 99.0;
	const int num_threads = 1024;
	int num_blocks;
	int deviceId;
	int numberOfSMs;
	cudaGetDevice(&deviceId);
	cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
	printf("Device ID: %d\tNumber of SMs: %d\n", deviceId, numberOfSMs);
	num_blocks=numberOfSMs;
	dim3 block_num(num_blocks,1,1);
	dim3 threads_per_block(num_threads,1,1);
	cudaMallocManaged(&a, size*sizeof(float));
	cudaMemcpy(a, input_vec, size*sizeof(float),cudaMemcpyHostToDevice);
	cudaMallocManaged(&gpu_min_vec, num_blocks*sizeof(float));
	cudaMemPrefetchAsync(gpu_min_vec, num_blocks*sizeof(float), deviceId);
	gpu_min_func<<<block_num,threads_per_block,num_threads*sizeof(float)>>> (a, gpu_min_vec, size);
	cudaDeviceSynchronize();
	cudaMemPrefetchAsync(gpu_min_vec, sizeof(gpu_min_vec), cudaCpuDeviceId);
	for(int i=0;i<num_blocks;i++){
		if(gpu_min_vec[i]<gpu_min){
			gpu_min = gpu_min_vec[i];
		}
	}
        return(gpu_min);






}

