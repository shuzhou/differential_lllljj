#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "reduce_var.cuh"
__global__ void gpu_var_func(float *input_vec, float *weight_vec, double *output_vec, double mean, long long size){
	extern __shared__ double sharedMem[];
	int stride = blockDim.x*gridDim.x;
	int index = blockIdx.x*blockDim.x+threadIdx.x;
	int tid = threadIdx.x;
	double temp;
	sharedMem[tid] = 0.0;
   	 __syncthreads();
	for(int i=index; i<size;i=i+stride){
		temp = (double)input_vec[i]-mean;
		sharedMem[tid] = sharedMem[tid]+temp*temp*(double)weight_vec[i];
	}
	__syncthreads();
	for(int offset = blockDim.x/2; offset>0; offset>>=1){
		if(tid<offset){
                	sharedMem[tid]=sharedMem[tid]+sharedMem[tid+offset]; 
		}
		 __syncthreads();
	}
	if(threadIdx.x==0){
		output_vec[blockIdx.x] = sharedMem[0];
	}
}
double reduce_var(float *input_vec, float *weight_vec, long long size, double mean){
	float *a;
	float *weight;
	double *gpu_sum_vec;
	double sum_value = 0.0;
	const int num_threads = 1024;
	int num_blocks;
	int deviceId;
	int numberOfSMs;
	cudaGetDevice(&deviceId);
	cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
	num_blocks=numberOfSMs;
	dim3 block_num(num_blocks,1,1);
	dim3 threads_per_block(num_threads,1,1);
	cudaMallocManaged(&a, size*sizeof(float));
	cudaMallocManaged(&weight, size*sizeof(float));
	cudaMemcpy(a, input_vec, size*sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(weight, weight_vec, size*sizeof(float),cudaMemcpyHostToDevice);
	cudaMallocManaged(&gpu_sum_vec, num_blocks*sizeof(double));
	cudaMemPrefetchAsync(gpu_sum_vec, num_blocks*sizeof(double), deviceId);
	gpu_var_func<<<block_num,threads_per_block, num_threads*sizeof(double)>>> (a, weight, gpu_sum_vec, mean, size);
	cudaDeviceSynchronize();
	cudaMemPrefetchAsync(gpu_sum_vec, sizeof(gpu_sum_vec), cudaCpuDeviceId);
	for(int i=0;i<num_blocks;i++){
		sum_value = sum_value+gpu_sum_vec[i];
	}
        return(sum_value);






}

