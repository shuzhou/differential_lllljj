from ROOT import TMVA, TFile, TTree, TCut, TString
#DNN Library Pytorch
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS 
import swats
#numpy, pandas, matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from importlib import reload
import pandas as pd
import random
from array import array
#ML library
import pickle
import scikitplot as skplt
import seaborn as sns
import sklearn
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.utils import shuffle
#XGBoost Library
import xgboost as xgb
from xgboost import XGBClassifier
from xgboost import plot_tree
#from IPython import display
#import graphviz
#Cuda Library
from numba import cuda
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
from math import sqrt
import gc 
from scipy.stats import poisson
import multiprocessing
import time
import h5py

import cuda_guass_normal


from .model import *
from .train import *
from .preprocess import *
from .network_core import *




    
    
    
def train_n_fold_new(num_fold, out_list, net_parameters, model_name, data_name, config_name, batch_size, max_epoch, patient):
    step_list = getTrainStep_new(out_list, num_fold)

    start_time = time.time()
    weight_name = out_list["weight_name"]
    drop_variable = out_list["drop_variable"]
    mean_vec = out_list["mean_vec"]
    var_vec = out_list["var_vec"]
    for i in range(num_fold):
        print("start section ",i)
        model_file = model_name+str(i)+".pt"
        dataTrainSample, dataTrainWeight,dataTrainLabel, dataTestSample,dataTestWeight, dataTestLabel= getTrain_Test_Sample_new(out_list, step_list, i, drop_variable)
    
    
        #prepare signal and background arrays for check
       
        #normalize input features
        print("Normalizing input features:")
        reload(cuda_guass_normal)
        dataTrainSample1=cuda_guass_normal.guass_normal_cuda(dataTrainSample, mean_vec, var_vec)
        dataTestSample1=cuda_guass_normal.guass_normal_cuda(dataTestSample, mean_vec, var_vec)
        #create data loader
        num_cpu = net_parameters["num_cpu"]
        print(num_cpu-1," cores will be used")
        drv.init()
        print("Loading dataloaders")
        # load DNN model and init weights
        print("Creating models")
        #model = bbll.Net(dim).to(device)
        my_net = core(net_parameters)
        print("Trainning model")
        my_net.train(dataTrainSample1, dataTrainLabel, dataTrainWeight, dataTestSample1, 
                     dataTestLabel, dataTestWeight, batch_size, 10000, max_epoch, patient)
    
        my_net.save_model(model_file)
        
        if(out_list["num_class"] !=2):
            continue
 
        print("Calculate train AUC: ")
    
        
        TrainScore=my_net.predict(dataTrainSample1, 10000)
        fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTrainLabel,TrainScore,sample_weight=dataTrainWeight)
        roc_auc = sklearn.metrics.auc(fpr, tpr)
        print("Train AUC is: ", roc_auc)
    
    
        print("Calculate test AUC: ")
        TestScore=my_net.predict(dataTestSample1, 10000)
        fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTestLabel,TestScore,sample_weight=dataTestWeight)
        roc_auc = sklearn.metrics.auc(fpr, tpr)
        print("Test AUC is: ", roc_auc)
                    
      
    
    print("Training finished, total time cost is: ", time.time()-start_time)
    data_file = h5py.File(data_name,'w')
    data_file["mean_vec"] = mean_vec
    data_file["var_vec"] = var_vec
    data_file.close()
    print("training normalization vectors are saved to "+data_name)
    training_parameters = net_parameters
    training_parameters["training_variable"] = out_list["training_variable"]
    training_parameters["ispDNN"] = out_list["ispDNN"]
    training_parameters["tag_variable"] = out_list["tag_variable"]
    config_f = open(config_name,'w')
    config_f.write(str(training_parameters))
    config_f.close()
    print("training settings are saved to"+config_name)
    
    
    
def calculate_importance(num_fold, dim, signal_df_pos, background_df_pos, mean_vec, var_vec, net_parameters, model_name, drop_variable, weight_name, batch_size, max_epoch, patient,is_shuffle, shuffle_num):
    signal_step, background_step = getTrainStep(signal_df_pos, background_df_pos, num_fold, weight_name)
    # scale up signal weights to equal total background yield 
    signal_ratio=signal_df_pos[weight_name].sum()/background_df_pos[weight_name].sum()
    print('signal background ratio is:', signal_ratio)

    signal_df_pos[weight_name]=signal_df_pos[weight_name]/signal_ratio
    start_time = time.time()
    roc_auc_train=0
    roc_auc_test=0
    for i in range(num_fold):
        print("start section ",i)
        model_file = model_name+str(i)+".pt"
        dataTrainSample, dataTrainWeight,dataTrainLabel, dataTestSample,dataTestWeight, dataTestLabel= getTrain_Test_Sample(signal_df_pos, background_df_pos, signal_step, background_step, i, drop_variable, weight_name)
    
        #normalize input features
        print("Normalizing input features:")
        reload(cuda_guass_normal)
        dataTrainSample1=cuda_guass_normal.guass_normal_cuda(dataTrainSample, mean_vec, var_vec)
        dataTestSample1=cuda_guass_normal.guass_normal_cuda(dataTestSample, mean_vec, var_vec)
        if(is_shuffle==1):
            np.random.shuffle(dataTrainSample1[:,shuffle_num])
            np.random.shuffle(dataTestSample1[:,shuffle_num])
            
        num_cpu = multiprocessing.cpu_count()-4
        #create data loader
        print(num_cpu-1," cores will be used")
        drv.init()
        print("Loading dataloaders")
        # load DNN model and init weights
        print("Creating models")
        #model = bbll.Net(dim).to(device)
        my_net = core(net_parameters)
        print("Trainning model")
        my_net.train(dataTrainSample1, dataTrainLabel, dataTrainWeight, dataTestSample1, 
                     dataTestLabel, dataTestWeight, batch_size, 10000, max_epoch, patient)
    
        my_net.save_model(model_file)
        print("Appling model to whole signal and background dataset")
        
    
        print("Calculate train AUC: ")
    
    
        TrainScore=my_net.predict(dataTrainSample1, 10000)
        fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTrainLabel,TrainScore,sample_weight=dataTrainWeight)
        roc_auc = sklearn.metrics.auc(fpr, tpr)
        roc_auc_train=roc_auc_train+roc_auc
        print("Train AUC is: ", roc_auc)
    
    
        print("Calculate test AUC: ")
        TestScore=my_net.predict(dataTestSample1, 10000)
        fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTestLabel,TestScore,sample_weight=dataTestWeight)
        roc_auc = sklearn.metrics.auc(fpr, tpr)
        roc_auc_test=roc_auc_test+roc_auc
        print("Test AUC is: ", roc_auc)
        print("Training finished, total time cost is: ", time.time()-start_time)
    roc_auc_train=roc_auc_train/num_fold
    roc_auc_test=roc_auc_test/num_fold
    return(roc_auc_train,roc_auc_test)


def calculate_importance_pDNN(num_fold, dim, signal_df_pos, background_df_pos, mean_vec, var_vec, net_parameters, model_name, drop_variable, weight_name, batch_size, max_epoch, patient,is_shuffle, shuffle_num, tag_num,mass_points):
    signal_step, background_step = getTrainStep(signal_df_pos, background_df_pos, num_fold, weight_name)
    # scale up signal weights to equal total background yield 
    signal_ratio=signal_df_pos[weight_name].sum()/background_df_pos[weight_name].sum()
    print('signal background ratio is:', signal_ratio)

    signal_df_pos[weight_name]=signal_df_pos[weight_name]/signal_ratio
    start_time = time.time()
    num_mass_points = len(mass_points)
    roc_auc_train = np.zeros((1,num_mass_points))
    roc_auc_test = np.zeros((1,num_mass_points))
    roc_auc_train_total = 0
    roc_auc_test_total = 0
    for i in range(num_fold):
        print("start section ",i)
        model_file = model_name+str(i)+".pt"
        dataTrainSample, dataTrainWeight,dataTrainLabel, dataTestSample,dataTestWeight, dataTestLabel= getTrain_Test_Sample(signal_df_pos, background_df_pos, signal_step, background_step, i, drop_variable, weight_name)
    
        #normalize input features
        print("Normalizing input features:")
        reload(cuda_guass_normal)
        dataTrainSample_s = dataTrainSample.copy()
        dataTestSample_s = dataTestSample.copy()
        if(is_shuffle==1):
            np.random.shuffle(dataTrainSample_s[:,shuffle_num])
            np.random.shuffle(dataTestSample_s[:,shuffle_num])
        dataTrainSample1=cuda_guass_normal.guass_normal_cuda(dataTrainSample_s, mean_vec, var_vec)
        dataTestSample1=cuda_guass_normal.guass_normal_cuda(dataTestSample_s, mean_vec, var_vec)
            
        num_cpu = multiprocessing.cpu_count()-4
        #create data loader
        print(num_cpu-1," cores will be used")
        drv.init()
        print("Loading dataloaders")
        # load DNN model and init weights
        print("Creating models")
        #model = bbll.Net(dim).to(device)
        my_net = core(net_parameters)
        print("Trainning model")
        my_net.train(dataTrainSample1, dataTrainLabel, dataTrainWeight, dataTestSample1, 
                     dataTestLabel, dataTestWeight, batch_size, 10000, max_epoch, patient)
    
        my_net.save_model(model_file)
        print("Appling model to whole signal and background dataset")
        m_count = 0
        for masspoint in mass_points:
            print("Calculate train AUC for mass point ",masspoint,"GeV:")
            reload(cuda_guass_normal)
            dataTrainSample_m = dataTrainSample_s[np.where(dataTrainSample[:,tag_num]==masspoint)]
            dataTrainWeight_m = dataTrainWeight[np.where(dataTrainSample[:,tag_num]==masspoint)]
            dataTrainLabel_m = dataTrainLabel[np.where(dataTrainSample[:,tag_num]==masspoint)]
            dataTrainSample_m1 = cuda_guass_normal.guass_normal_cuda(dataTrainSample_m, mean_vec, var_vec)
            TrainScore_m = my_net.predict(dataTrainSample_m1, 10000)
            fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTrainLabel_m,TrainScore_m,sample_weight=dataTrainWeight_m)
            roc_auc = sklearn.metrics.auc(fpr, tpr)
            roc_auc_train[0,m_count] = roc_auc_train[0,m_count]+roc_auc/num_fold
            print("Train AUC at mass point ",masspoint,"GeV is: ", roc_auc)
            
            print("Calculate Test AUC for mass point ",masspoint,"GeV:")
            dataTestSample_m = dataTestSample_s[np.where(dataTestSample[:,tag_num]==masspoint)]
            dataTestWeight_m = dataTestWeight[np.where(dataTestSample[:,tag_num]==masspoint)]
            dataTestLabel_m = dataTestLabel[np.where(dataTestSample[:,tag_num]==masspoint)]
            dataTestSample_m1 = cuda_guass_normal.guass_normal_cuda(dataTestSample_m, mean_vec, var_vec)
            TestScore_m = my_net.predict(dataTestSample_m1, 10000)
            fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTestLabel_m,TestScore_m,sample_weight=dataTestWeight_m)
            roc_auc = sklearn.metrics.auc(fpr, tpr)
            roc_auc_test[0,m_count] = roc_auc_test[0,m_count]+roc_auc/num_fold
            print("Test AUC at mass point ",masspoint,"GeV is: ", roc_auc)
            m_count = m_count+1
            
            
            
        
    
        print("Calculate train AUC for all samples: ")
    
    
        TrainScore=my_net.predict(dataTrainSample1, 10000)
        fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTrainLabel,TrainScore,sample_weight=dataTrainWeight)
        roc_auc = sklearn.metrics.auc(fpr, tpr)
        roc_auc_train_total = roc_auc_train_total + roc_auc
        print("Train AUC is: ", roc_auc)
    
    
        print("Calculate test AUC: ")
        TestScore=my_net.predict(dataTestSample1, 10000)
        fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTestLabel,TestScore,sample_weight=dataTestWeight)
        roc_auc = sklearn.metrics.auc(fpr, tpr)
        roc_auc_test_total = roc_auc_test_total + roc_auc
        print("Test AUC is: ", roc_auc)
        print("Training finished, total time cost is: ", time.time()-start_time)
    roc_auc_train_total=roc_auc_train_total/num_fold
    roc_auc_test_total=roc_auc_test_total/num_fold
    return(roc_auc_train,roc_auc_test,roc_auc_train_total,roc_auc_test_total)

    
    
    
    

                    
      
    
   
    
    
