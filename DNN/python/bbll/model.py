import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

class Mish(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        return x *( torch.tanh(F.softplus(x)))

class Net(nn.Module):
    
    def __init__(self,dim):
        super().__init__()
        self.fc1 = nn.utils.weight_norm(nn.Linear(dim, 100),name='weight')
        self.relu1 = Mish()
        self.dout = nn.Dropout(0.2)
        self.fc2 = nn.utils.weight_norm(nn.Linear(100, 100),name='weight')
        self.relu2 = Mish()
        self.fc3 = nn.utils.weight_norm(nn.Linear(100, 100),name='weight')
        self.relu3 = Mish()
        self.fc4 = nn.utils.weight_norm(nn.Linear(100, 100),name='weight')
        self.relu4 = Mish()
        self.out = nn.utils.weight_norm(nn.Linear(100, 1),name='weight')
        self.out_act = nn.Sigmoid()
        
    def forward(self, input_):
        a1 = self.fc1(input_)
        h1 = self.relu1(a1)
        dout = self.dout(h1)
        a2 = self.fc2(dout)
        h2 = self.relu2(a2)
        a3 = self.fc3(h2)
        h3 = self.relu3(a3)
        a4 = self.fc4(h3)
        h4 = self.relu4(a4)
        a5 = self.out(h4)
        y = self.out_act(a5)
        return y
    
class ResNet(nn.Module):
    
    def __init__(self,dim):
        super().__init__()
        self.fc1 = nn.utils.weight_norm(nn.Linear(dim, 100),name='weight')
        self.relu1 = Mish()
        self.dout = nn.Dropout(0.2)
        self.fc2 = nn.utils.weight_norm(nn.Linear(100, 100),name='weight')
        self.relu2 = Mish()
        self.fc3 = nn.utils.weight_norm(nn.Linear(100, 100),name='weight')
        self.relu3 = Mish()
        self.fc4 = nn.utils.weight_norm(nn.Linear(100, 100),name='weight')
        self.relu4 = Mish()
        self.out = nn.utils.weight_norm(nn.Linear(100, dim),name='weight')
        self.out_act = Mish()
        
    def forward(self, input_):
        a1 = self.fc1(input_)
        h1 = self.relu1(a1)
        dout = self.dout(h1)
        a2 = self.fc2(dout)
        h2 = self.relu2(a2)
        a3 = self.fc3(h2)
        h3 = self.relu3(a3)
        a4 = self.fc4(h3)
        h4 = self.relu4(a4)
        a5 = self.out(h4)
        y = self.out_act(a5+input_)
        return y
    
class simple_layer(nn.Module):
    
    def __init__(self,input_dim, output_dim, use_dropout, drop_rate):
        super().__init__()
        self.fc = nn.utils.weight_norm(nn.Linear(input_dim, output_dim),name='weight')
        self.use_drop = use_dropout 
        if(self.use_drop == True):
            self.dout = nn.Dropout(drop_rate)
        self.act = Mish()
        #self.act = nn.ReLU()
        
    def forward(self, input_):
        z = self.fc(input_)
        a = self.act(z)
        dout = a
        if(self.use_drop == True):
            dout = self.dout(a)
        return dout
    
    
    
class output_layer(nn.Module):
    def __init__(self, input_dim, num_class):
        super().__init__()
        self.num_class = num_class
        if(self.num_class ==2):
            self.fc = nn.utils.weight_norm(nn.Linear(input_dim, 1),name='weight')
            self.act = nn.Sigmoid()
        else:
            self.fc = nn.utils.weight_norm(nn.Linear(input_dim, num_class),name='weight')
        
    def forward(self, input_):
        z = self.fc(input_)
        a = z
        if(self.num_class == 2):
            a = self.act(z)
        return a
    
class ResNet_module(nn.Module):
    def __init__(self, dim, depth, dropout_rate):
        super().__init__()
        self.layers = []
        self.dim = dim 
        self.dropout_rate = dropout_rate
        self.depth = depth
        for l in range(depth-1):
            if(dropout_rate == 0):
                self.layers.append(simple_layer(dim,dim,False, self.dropout_rate))
            else:
                self.layers.append(simple_layer(dim,dim,True, self.dropout_rate))
        self.fcs = nn.Sequential(*self.layers)
        self.out = nn.utils.weight_norm(nn.Linear(dim, dim),name='weight')
        self.out_act = Mish()
        
    def forward(self, input_):
        fcs = self.fcs(input_)
        out_z = self.out(fcs)
        out_act = self.out_act(input_+out_z)
        
        return out_act

    
def generate_parameter(dim_list, drop_out, drop_out_rate):
    parameter_list = {}
    for l in range(len(dim_list)-1):
        parameter_list["input_dim"+str(l+1)] = dim_list[l]
        parameter_list["output_dim"+str(l+1)] = dim_list[l+1]
        parameter_list["use_drop_out_layer"+str(l+1)] = drop_out[l]
        parameter_list["dropout_rate"+str(l+1)] = drop_out_rate[l]
    parameter_list["output_layer_input"] = dim_list[len(dim_list)-1]
    parameter_list["Total_layer"] = len(dim_list)
    return parameter_list


def generate_linear_model(parameter_list, device):
    layers = []
    for l in range(parameter_list["Total_layer"]-1):
        layers.append(simple_layer(parameter_list["input_dim"+str(l+1)],parameter_list["output_dim"+str(l+1)],
                                   parameter_list["use_drop_out_layer"+str(l+1)], parameter_list["dropout_rate"+str(l+1)]))
    layers.append(output_layer(parameter_list["output_layer_input"], parameter_list["num_class"]))
    model = nn.Sequential(*layers).to(device)
    return model


def generate_ResNet(parameters, device):
    layers = []
    layers.append(simple_layer(parameters["input_dim"], parameters["Res_dim"],
                               False, 0.0))
    for l in range(parameters["num_res"]):
        layers.append(ResNet_module(parameters["Res_dim"], parameters["Res_depth"], parameters["dropout_rate"]))
    layers.append(output_layer(parameters["Res_dim"], parameters["num_class"]))
    model = nn.Sequential(*layers).to(device)
    return model

    
    

