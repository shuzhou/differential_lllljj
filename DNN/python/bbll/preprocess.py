import numpy as np
import pandas as pd
import sklearn
from sklearn.utils import shuffle
import cuda_guass_normal
import gc 
import matplotlib.pyplot as plt
import seaborn as sns
from ROOT import TMVA, TFile, TTree, TCut, TString
import torch 
from importlib import reload

def fill_missing_value(dataPd,weight_name,missing_value):
    weight_pd = dataPd[weight_name]
    weight_np = weight_pd.values
    data_pd = dataPd.drop(columns=[weight_name])
    data_np = data_pd.values
    total = cuda_guass_normal.cuda_add(weight_np)
    mean_vec = cuda_guass_normal.cuda_mean(data_np, weight_np, total)
    del data_np
    del weight_np
    gc.collect()
    i=0
    for col in dataPd.columns:
        print(col)
        if(col==weight_name): continue
        dataPd[col].replace(
            to_replace=[missing_value],
            value=mean_vec[i],
            inplace=True
        )
        print(mean_vec[i])
        i=i+1
    return(dataPd)

        
def find_mean_value(dataPd, weight_name):
    weight_pd = dataPd[weight_name]
    weight_np = weight_pd.values
    data_pd = dataPd.drop(columns=[weight_name])
    data_np = data_pd.values
    total = cuda_guass_normal.cuda_add(weight_np)
    mean_vec = cuda_guass_normal.cuda_mean(data_np, weight_np, total)
    del data_np
    del weight_np
    gc.collect()
    return(mean_vec)


def fill_missing_value_mean(dataPd,weight_name,missing_value, mean_vec):
    i=0
    for col in dataPd.columns:
        print(col)
        if(col==weight_name): continue
        dataPd[col].replace(
            to_replace=[missing_value],
            value=mean_vec[i],
            inplace=True
        )
        print(mean_vec[i])
        i=i+1
    return(dataPd)



def sample_value(leng, target_df, weight_df, variable_name, weight_name):
    target_np = target_df[variable_name].values
    weight_np = weight_df[weight_name].values
    sum_weight = sum(weight_np)
    weight_np = weight_np/sum_weight
    result_np = []
    result_np = np.random.choice(target_np, p = weight_np, replace=True,size=leng)
    return(result_np)



def getTrainStep(signal_df, background_df, num_fold, weight_name):
    signal_size=signal_df[weight_name].count()
    background_size=background_df[weight_name].count()
    signal_step = int(signal_size/num_fold)
    background_step = int(background_size/num_fold)
    return(signal_step, background_step)


def getTrainStep_new(out_list, num_fold):
    train_step_list = {}
    weight_name = out_list["weight_name"]
    for i in range(out_list["sample_num"]):
        temp_size = out_list["dataframes_pos"]["sample"+str(i)][weight_name].count()
        train_step_list["sample"+str(i)] = int(temp_size/num_fold)
    
        
    return(train_step_list)


def getTrain_Test_Sample(signal_df_pos, background_df_pos, signal_step, background_step, fold, drop_variable, weight_name):
    i = fold
    signal_size=signal_df_pos[weight_name].count()
    background_size=background_df_pos[weight_name].count()
    signal_start = i*signal_step
    signal_end = (i+1)*signal_step
    background_start = i*background_step
    background_end = (i+1)*background_step
    print(signal_start, signal_end)
    print(background_start, background_end)
    #prepare training and testing set
    signal_df_test = signal_df_pos.iloc[signal_start:signal_end]
    signal_df_train = signal_df_pos.iloc[0:signal_start].append(signal_df_pos.iloc[signal_end:signal_size-1])
    background_df_test = background_df_pos.iloc[background_start:background_end]
    background_df_train = background_df_pos.iloc[0:background_start].append(background_df_pos.iloc[background_end:background_size-1])
    train_ratio=signal_df_train[weight_name].sum()/background_df_train[weight_name].sum()
    test_ratio=signal_df_test[weight_name].sum()/background_df_test[weight_name].sum()
    print(train_ratio)
    print(test_ratio)
    data_df_train=signal_df_train.append(background_df_train)
    data_df_test=signal_df_test.append(background_df_test)
    data_df_train=shuffle(data_df_train)
    data_df_test=shuffle(data_df_test)
    data_train_label=data_df_train["label"]
    data_train_weight=data_df_train[weight_name]
    data_train_data=data_df_train.drop(columns=drop_variable)
    data_test_label=data_df_test["label"]
    data_test_weight=data_df_test[weight_name]
    data_test_data=data_df_test.drop(columns=drop_variable)
    #prepare features, weight and label arrays
    dataTrainSample=data_train_data.values
    dataTestSample=data_test_data.values
    dataTrainWeight=data_train_weight.values
    dataTestWeight=data_test_weight.values
    dataTrainLabel=data_train_label.values
    dataTestLabel=data_test_label.values
    return(dataTrainSample, dataTrainWeight, dataTrainLabel, dataTestSample, dataTestWeight, dataTestLabel)



def getTrain_Test_Sample_new(out_list, step_list, fold, drop_variable):
    i = fold
    weight_name = out_list["weight_name"]
    for l in range(out_list["sample_num"]):
        temp_df = out_list["dataframes_pos"]["sample"+str(l)]
        temp_start = i*step_list["sample"+str(l)]
        temp_end = (i+1)*step_list["sample"+str(l)]
        temp_df_test = temp_df.iloc[temp_start:temp_end]
        temp_df_train = temp_df.iloc[0:temp_start].append(temp_df[temp_end:])
        if(l == 0):
            data_df_train = temp_df_train
            data_df_test = temp_df_test
        else:
            data_df_train = data_df_train.append(temp_df_train)
            data_df_test = data_df_test.append(temp_df_test)
    data_df_train = shuffle(data_df_train)
    data_df_test = shuffle(data_df_test)
    data_train_label=data_df_train["label"]
    data_train_weight=data_df_train[weight_name]
    data_train_data=data_df_train.drop(columns=drop_variable)
    data_test_label=data_df_test["label"]
    data_test_weight=data_df_test[weight_name]
    data_test_data=data_df_test.drop(columns=drop_variable)
    #prepare features, weight and label arrays
    dataTrainSample=data_train_data.values
    dataTestSample=data_test_data.values
    dataTrainWeight=data_train_weight.values
    dataTestWeight=data_test_weight.values
    dataTrainLabel=data_train_label.values
    dataTestLabel=data_test_label.values
    
            
    return(dataTrainSample, dataTrainWeight, dataTrainLabel, dataTestSample, dataTestWeight, dataTestLabel)





def odd_even_sort(name_list,value):
    flag=1
    dim=len(name_list)
    while(flag==1):
        i=0
        flag=0
        while(2*i+1<dim):
            if(value[2*i]>value[2*i+1]):
                temp_v = value[2*i]
                temp_n = name_list[2*i]
                value[2*i]=value[2*i+1]
                name_list[2*i]=name_list[2*i+1]
                value[2*i+1]=temp_v
                name_list[2*i+1]=temp_n
                flag=1
                
            i=i+1
        i=0   
        while(2*i+2<dim):
            if(value[2*i+1]>value[2*i+2]):
                temp_v = value[2*i+1]
                temp_n = name_list[2*i+1]
                value[2*i+1]=value[2*i+2]
                name_list[2*i+1]=name_list[2*i+2]
                value[2*i+2]=temp_v
                name_list[2*i+2]=temp_n
                flag=1
                
            i=i+1
    return(name_list,value)

def get_ranking_plot(auc_np,varname,title,xlabel,save_path):
    auc_np = auc_np.reshape(-1)    
    auc_np=auc_np/abs(max(auc_np))*100
    varname_m = varname.copy()
    varname_m,auc_np = odd_even_sort(varname_m,auc_np)
    
    plt.barh(range(len(auc_np)), auc_np,tick_label = varname_m)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.tight_layout()
    figure_fig = plt.gcf()
    figure_fig.savefig(save_path, format='pdf', dpi=500)
    plt.close('all')
    print("ranking plot saved")


def load_dataFrame(file_path, file_name, ntuple_name, variable_list):
    File_temp = TFile.Open(file_path+"/"+file_name)
    ntuple_temp = File_temp.Get(ntuple_name)
    Array_temp = ntuple_temp.AsMatrix(variable_list)
    pd_temp = pd.DataFrame(data = Array_temp, columns = variable_list)
    return pd_temp

def set_cut(dataframe, replace_name, name , cut_str):
    cut_str_temp = cut_str.replace(replace_name, name)
    pd_temp = dataframe
    dataframe_new = pd_temp[eval(cut_str_temp)]
    return dataframe_new


def get_correlation_matrix(values_np, weight_np, var_list, save_path, filename):
    cuda_cor=cuda_guass_normal.cuda_correlation(values_np, weight_np, var_list)
    plt.figure(figsize=(12, 12))
    sns.set(font_scale=2)
    sns.heatmap(cuda_cor, cmap='coolwarm', annot=False, annot_kws={"size": 20})
    plt.tight_layout()
    figure_fig = plt.gcf()  # 'get current figure'
    figure_fig.savefig(save_path+"/"+filename, format='pdf', dpi=500)
    plt.close('all')
    
    

def create_dataset(parameter_list):
    sample_num = parameter_list["sample_num"]
    weight_name = parameter_list["weight_name"]
    variable_list = parameter_list["variable_list"]
    sample_path = parameter_list["sample_path"]
    ntuple_name = parameter_list["ntuple_name"]
    label_list = parameter_list["label_list"]
    do_reweight = parameter_list["do_reweight"]
    ispDNN = parameter_list["ispDNN"]
    figures_path = parameter_list["figures_path"]
    do_shuffle = parameter_list["do_shuffle"]
    num_class = len(set(label_list))
    if(do_reweight):
        ratio_list = parameter_list["ratio_list"]
    File_list = {}
    ntuple_list = {}
    dataFrame_list = {}
    dataFrame_pos_list = {}
    yield_list = {}
    yield_list_pos = {}
    output = {}
    reload(cuda_guass_normal)
    cut_num = parameter_list["cut_num"]
    unused_variable = parameter_list["unused_variable"].copy()
    unused_variable.append("label")
    print("Start loading files")
    for i in range(sample_num):
        sample_name = parameter_list["sample_name"+str(i+1)]
        dataFrame_list["sample"+str(i)] = load_dataFrame(sample_path, sample_name, ntuple_name, variable_list)
    
    for i in range(cut_num):
        if((parameter_list["cut"+str(i+1)+"_type"] == "ALL")|(parameter_list["cut"+str(i+1)+"_type"] == "all")):
            for l in range(sample_num):
                pd_temp = dataFrame_list["sample"+str(l)]
                cut_str = parameter_list["cut"+str(i+1)]
                pd_new = set_cut(pd_temp, "DF", "pd_temp", cut_str)
                dataFrame_list["sample"+str(l)] = pd_new
        else:
            for num in parameter_list["cut"+str(i+1)+"_samples"]:
                pd_temp = dataFrame_list["sample"+str(num-1)]
                cut_str = parameter_list["cut"+str(i+1)]
                pd_new = set_cut(pd_temp, "DF", "pd_temp", cut_str)
                dataFrame_list["sample"+str(num-1)] = pd_new
    
    if(ispDNN):
        print("Sample tag variable: "+parameter_list["tag_variable"])
        target_pd = dataFrame_list["sample"+str(parameter_list["target_sample"][0]-1)]
        for i in range(1, len(parameter_list["target_sample"])):
            target_pd = target_pd.append(dataFrame_list["sample"+str(parameter_list["target_sample"][i]-1)])
        for num in parameter_list["sample_to_fill"]:
            pd_temp = dataFrame_list["sample"+str(num-1)]
            result_np = sample_value(len(pd_temp[parameter_list["tag_variable"]].values), target_pd, target_pd,
                         parameter_list["tag_variable"],weight_name)
            temp_np = pd_temp.values
            tag_loc = variable_list.index(parameter_list["tag_variable"])
            temp_np[:,tag_loc] = result_np
            dataFrame_list["sample"+str(num-1)] = pd.DataFrame(data = temp_np, columns = variable_list)
    for i in range(sample_num):
        yield_list["sample"+str(i)] = dataFrame_list["sample"+str(i)][weight_name].sum()
        dataFrame_list["sample"+str(i)]["label"] = label_list[i] # create label to each sample
    if(parameter_list["get_correlation_matrix"]):
        for i in range(sample_num):
            pd_temp = dataFrame_list["sample"+str(i)]
            pd_temp_cor = pd_temp.drop(columns = unused_variable)
            var_name = pd_temp_cor.columns.values.tolist()
            np_temp = pd_temp_cor.values
            np_weight_temp = pd_temp[weight_name].values
            get_correlation_matrix(np_temp, np_weight_temp, var_name, figures_path, "correlation-sample"+str(i)+".pdf")
            print("correlation matrix saved at: "+figures_path)
        
    
    
    dataNormal_pre = dataFrame_list["sample0"]
    for i in range(1,sample_num):
        dataNormal_pre = dataNormal_pre.append(dataFrame_list["sample"+str(i)])
    dataNormal_weight_df = dataNormal_pre[weight_name]
    dataNormal_df = dataNormal_pre.drop(columns=unused_variable)
    output["mean_vec"], output["var_vec"] = cuda_guass_normal.cuda_mean_var(dataNormal_df.values,
                                                                            dataNormal_weight_df.values)
    for i in range(sample_num):
        pd_temp = dataFrame_list["sample"+str(i)]
        pd_temp_pos = pd_temp[pd_temp[weight_name]>0]
        dataFrame_pos_list["sample"+str(i)] = pd_temp_pos
        yield_list_pos["sample"+str(i)] = dataFrame_pos_list["sample"+str(i)][weight_name].sum()
        if(do_shuffle):
            dataFrame_pos_list["sample"+str(i)] = shuffle(dataFrame_pos_list["sample"+str(i)])
        
    if(do_reweight):
        for i in range(sample_num):
            pd_temp = dataFrame_pos_list["sample"+str(i)]
            print("sample"+str(i+1)+" yield before reweight is: "+str(pd_temp[weight_name].sum()))
            pd_temp[weight_name] = pd_temp[weight_name]/yield_list_pos["sample"+str(i)]*yield_list_pos["sample0"]*ratio_list[i]
            dataFrame_pos_list["sample"+str(i)] = pd_temp
            print("sample"+str(i+1)+" yield after reweight is: "+str(pd_temp[weight_name].sum()))
    pd_temp = dataFrame_list["sample"+str(0)]
    pd_temp_cor = pd_temp.drop(columns = unused_variable)
    training_var_name = pd_temp_cor.columns.values.tolist()
    output["training_variable"] = training_var_name
    output["dataframes"] = dataFrame_list
    output["dataframes_pos"] = dataFrame_pos_list
    output["yield_list"] = yield_list
    output["num_class"] = num_class
    output["weight_name"] = weight_name
    output["sample_num"] = sample_num
    output["drop_variable"] = unused_variable
    output["ispDNN"] = ispDNN
    output["tag_variable"] = parameter_list["tag_variable"]
    print("dataset is successfully constructed!")
    return(output)




def one_hot_embedding(labels, num_classes):
    if(labels.type()!="torch.LongTensor"):
        labels = labels.type("torch.LongTensor")
    y = torch.eye(num_classes) 
    return y[labels] 




 












    
    
    
