    #include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"
#include"THStack.h"
#include "TLatex.h"
#include"AtlasStyle.C"
#include"AtlasLabels.C"
#include "AtlasUtils.C"



//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void com_sys2d() {
    // Define two gaussian histograms. Note the X and Y title are defined
    // at booking time using the convention "Hist_title ; X_title ; Y_title"
    TFile* file1 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/systematic/sys_QCD.root");
    //TFile* file2 = TFile::Open("/atlas/data18a/shuzhouz/VBSZZ4l/temp/truth_QCD.root");
    
  // SetAtlasStyle();
    gROOT->LoadMacro("AtlasUtils.C");
    double range[24];
    for(int i=0;i<=15;i++){
        range[i]=20*i;
    }
    for(int i=16;i<=21;i++){
        range[i]=300+(i-15)*50;
    }
    for(int i=22;i<24;i++){
        range[i]=600+(i-21)*100;
    }
    
    
    TH2F *H1= new TH2F("H1","", 20,0, 800,20,-5,5);
     TH2F *H2= new TH2F("H2","", 20,0, 800,20,-5,5);
     TH2F *H3= new TH2F("H3","", 20,0, 800,20,-5,5);
    
    //TH1F *h0 = new TH1F("h0", "h0", 50, 0, 3000);
    
    TLatex latex;
    TTree* t1;
    TTree* t2;
    TTree* t3;
    double normal=0.01298;
    double weight1,weight2,weight3;
    double mjj1,mjj2,mjj3,detajj;
    double eta1,eta2,eta3,eta4,eta5,eta6;
    double ptj1,ptj2,ptj3;
    
    
   
    file1->GetObject("tree_NOMINAL",t1);
    file1->GetObject("tree_JET_GroupedNP_1__1up",t2);
    file1->GetObject("tree_JET_GroupedNP_1__1down",t3);
    
    
    
    t1->SetBranchAddress("MJJ", &mjj1);
    t1->SetBranchAddress("dEtaJJ", &eta1);
    t1->SetBranchAddress("weight", &weight1);
    t1->SetBranchAddress("PtJ1", &ptj1);
    t1->SetBranchAddress("EtaJ1", &eta4);
    t2->SetBranchAddress("MJJ", &mjj2);
    t2->SetBranchAddress("weight", &weight2);
    t2->SetBranchAddress("dEtaJJ", &eta2);
    t2->SetBranchAddress("PtJ1", &ptj2);
    t2->SetBranchAddress("EtaJ1", &eta5);
    t3->SetBranchAddress("MJJ", &mjj3);
    t3->SetBranchAddress("dEtaJJ", &eta3);
    t3->SetBranchAddress("weight", &weight3);
    t3->SetBranchAddress("PtJ1", &ptj3);
    t3->SetBranchAddress("EtaJ1", &eta6);
   // t3->SetBranchAddress("EtaJ2", &eta6);
    
    
    for(int i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        if(mjj1>300&&fabs(eta1)>2.0){
            
            H1->Fill(ptj1,eta4,weight1*normal);
        }
    }
    for(int i=0;i<t2->GetEntries();i++){
        t2->GetEntry(i);
        if(mjj2>300&&fabs(eta2)>2.0){
    
            H2->Fill(ptj2,eta5,weight2*normal);
       }
    }
    for(int i=0;i<t3->GetEntries();i++){
        t3->GetEntry(i);
        if(mjj3>300&&fabs(eta3)>2.0){
         
            H3->Fill(ptj3,eta6,weight3*normal);
        }
    }
    
    

    
    
    // Define the Canvas
    
    
    // Upper plot will be in pad1
    
    //h9->SetMinimum(0.1);  // Define Y ..
   // H1->SetMaximum(6);
   TH2F *h3 = (TH2F*)H2->Clone("H2");
    h3->Sumw2();
    //h3->Divide(H1);
    h3->SetStats(0);// No statistics on upper plot
    h3->GetYaxis()->SetTitle("#eta_{J_{1}}");
    h3->GetXaxis()->SetTitle("P_{T}^{J_{1}} (GeV)");
    //h3->GetYaxis()->SetTitleSize(8);
    //h3->GetXaxis()->SetTitleSize(15);
    h3->Draw("colz");
    
        // Draw h2 on top of h1
    ATLAS_LABEL(0.4,0.8);
    myText(0.42,0.73,1,"#sqrt{s}=13 TeV   #int Ldt=79.8 fb^{-1}");
    //myText(0.42,0.63,1,"A:Black, C:Blue");
    
    
    
    
    // Do not draw the Y axis label on the upper plot and redraw a small
    // axis instead, in order to avoid the first label (0) to be clipped.
    
    
    // lower plot will be in pad
    
}
