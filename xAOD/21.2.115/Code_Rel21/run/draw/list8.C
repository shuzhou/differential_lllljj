#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"


//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void list8() {
   
    TFile* file = TFile::Open("/atlas/data18a/shuzhouz/VBSZZ4l/Code_Rel21/run/output/input6.list/data-tree_output/xAOD.root");
    TTree* T1;
   
    double ltype[4];
    double liso[4];
    double ld0[4];
    double lid[4];
    double etaj1,etaj2,detajj,mjj;
    
    int ngood;
    int nbad;
    int count=0;
    int count1=0;
    int count2=0;
    ULong64_t event;
    int nj;
    int run;
    
    
    int j=0;
    int k=0;
    
    
    file->GetObject("tree_NOMINAL",T1);
    //file->GetObject("sumweight",t2);
    T1->SetBranchAddress("EtaJ1", &etaj1);
    T1->SetBranchAddress("run", &run);
    T1->SetBranchAddress("nJet", &nj);
    T1->SetBranchAddress("event", &event);
    T1->SetBranchAddress("EtaJ2", &etaj2);
    T1->SetBranchAddress("dEtaJJ", &detajj);
    T1->SetBranchAddress("MJJ", &mjj);
    T1->SetBranchAddress("L1type", &ltype[0]);
    T1->SetBranchAddress("L2type", &ltype[1]);
    T1->SetBranchAddress("L3type", &ltype[2]);
    T1->SetBranchAddress("L4type", &ltype[3]);
    
    T1->SetBranchAddress("L1Iso", &liso[0]);
    T1->SetBranchAddress("L2Iso", &liso[1]);
    T1->SetBranchAddress("L3Iso", &liso[2]);
    T1->SetBranchAddress("L4Iso", &liso[3]);
    
    T1->SetBranchAddress("L1d0sig", &ld0[0]);
    T1->SetBranchAddress("L2d0sig", &ld0[1]);
    T1->SetBranchAddress("L3d0sig", &ld0[2]);
    T1->SetBranchAddress("L4d0sig", &ld0[3]);
    
    T1->SetBranchAddress("L1ID", &lid[0]);
    T1->SetBranchAddress("L2ID", &lid[1]);
    T1->SetBranchAddress("L3ID", &lid[2]);
    T1->SetBranchAddress("L4ID", &lid[3]);
    
    
    
    //t1->SetBranchAddress("numfake", &tw);
    for(int i=0;i<T1->GetEntries();i++){
        T1->GetEntry(i);
        if(event==73169){
            cout<<"MJJ: "<<mjj<<" dEtaJJ: "<<detajj<<" nJet: "<<nj<<endl;
            for(int j=0;j<4;j++){
                cout<<"Type: "<<ltype[j]<<" Iso: "<<liso[j]<<" d0Sig: "<<ld0[j]<<" ID: "<<lid[j]<<endl;
            }
            break;
            
        }
    }
    // cout<<count<<" "<<count1<<" "<<count2<<endl;
    
    
    
    
    
    
    
    
    
}