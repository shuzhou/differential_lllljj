#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"
#include"THStack.h"
#include "TLatex.h"
#include"AtlasStyle.C"
#include"AtlasLabels.C"
#include "AtlasUtils.C"



//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void truth-plot() {
    // Define two gaussian histograms. Note the X and Y title are defined
    // at booking time using the convention "Hist_title ; X_title ; Y_title"
    TFile* file1 = TFile::Open("/atlas/data18a/shuzhouz/VBSZZ4l/temp/tree_vbs.root");
    TFile* file2 = TFile::Open("/atlas/data18a/shuzhouz/VBSZZ4l/temp/truth_vbs.root");
    
  // SetAtlasStyle();
    gROOT->LoadMacro("AtlasUtils.C");
    
    TH1F *h1 = new TH1F("h1", "Fake factor;Pt (Gev); Lepton numbers", 50, 0, 3000);
    TH1F *h2 = new TH1F("h2", "", 50, 0, 3000);
    TH1F *h3 = new TH1F("h3", "h3", 50, 0, 3000);
    
    TLatex latex;
    TTree* t1;
    TTree* t2;
    TTree* t3;
    double normal=0.0001752;
    double weight1,weight2,weight3;
    double mjj1,mjj2,mjj3,detajj;
    
    
   
    file1->GetObject("Phase-space",t1);
    file1->GetObject("Fadiucal-Volum",t2);
    file1->GetObject("tree_NOMINAL",t3);
    
    
    
    t1->SetBranchAddress("MJJ", &mjj1);
    t1->SetBranchAddress("weight", &weight1);
    t2->SetBranchAddress("MJJ", &mjj2);
    t2->SetBranchAddress("weight", &weight2);
    t3->SetBranchAddress("MJJ", &mjj3);
    t3->SetBranchAddress("dEtaJJ", &detajj);
    t3->SetBranchAddress("weight", &weight3);
    
    
    for(int i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        h1->Fill(mjj1,weight1*normal);
    }
    for(int i=0;i<t2->GetEntries();i++){
        t2->GetEntry(i);
        h2->Fill(mjj2,weight2*normal);
    }
    for(int i=0;i<t3->GetEntries();i++){
        t3->GetEntry(i);
        if(weight3=-9999.0) continue;
        if(!(mjj3>300&&fabs(detajj)>2.0)) continue;
        h3->Fill(mjj3,weight3*normal);
    }
    

    
    
    // Define the Canvas
    TCanvas *c = new TCanvas("c", "canvas", 600, 600);
    
    // Upper plot will be in pad1
    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
    pad1->SetBottomMargin(0.05); // Upper and lower plot are joined
    // pad1->SetGridx();
   //pad1->SetLogy(1);         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();// pad1 becomes the current pad
    //h9->SetMinimum(0.1);  // Define Y ..
    //h9->SetMaximum(14000);
    h1->SetStats(0);// No statistics on upper plot
    h1->GetYaxis()->SetTitle("nEvents/ 5 Gev ");
    //h2->GetYaxis()->SetTitleSize(15);
    //h2->GetYaxis()->SetTitleOffset(0.8);
    //h2->Draw("ep");
                   // Draw h1
    h1->SetLineColor(kBlue);
    h1->SetLineWidth(2);
    h2->SetLineColor(kRed);
    h2->SetLineWidth(2);
    h3->SetLineColor(kGreen);
    h3->SetLineWidth(2);
    
        // Draw h2 on top of h1
    ATLAS_LABEL(0.4,0.8);
    myText(0.42,0.73,1,"#sqrt{s}=13 TeV   #int Ldt=79.8 fb^{-1}");
    
    TLegend *legend = new TLegend(0.6,0.6,0.85,0.85);
    
    //legend->AddEntry(h2,"Data(15-17)","P");
    legend->AddEntry(h1,"EWK in P.S","L");
    legend->AddEntry(h2,"EWK in F.V","L");
    legend->AddEntry(h3,"EWK at Reco level","L");
    legend->SetBorderSize(0);
    legend->SetFillColor(0);
    legend->Draw();
    
    
    // Do not draw the Y axis label on the upper plot and redraw a small
    // axis instead, in order to avoid the first label (0) to be clipped.
    
    
    // lower plot will be in pad
    c->cd();          // Go back to the main canvas before defining pad2
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.3);
    pad2->SetTopMargin(0);
    pad2->SetBottomMargin(0.26);
    pad2->SetGridx();
    pad2->SetGridy();// vertical grid
    pad2->Draw();
    pad2->cd();       // pad2 becomes the current pad
    
    // Define the ratio plot
    TH1F *h4 = (TH1F*)h2->Clone("h4");
    h4->SetLineColor(kBlack);
    h4->SetMinimum(0);  // Define Y ..
    h4->SetMaximum(1); // .. range
    h4->Sumw2();
    h4->SetStats(0);      // No statistics on lower plot
    h4->Divide(h1);
    
    h4->Draw();       // Draw the ratio plot
    TH1F *h5 = (TH1F*)h3->Clone("h5");
    h5->SetLineColor(kBlue);
    h5->Sumw2();
    h5->Divide(h2);
    h5->Draw("same");
    
    // h1 settings
    
    
    // Y axis h1 plot settings
    // h2 settings
    
    
    // Ratio plot (h3) settings
    h4->SetTitle(""); // Remove the ratio title
    
    // Y axis ratio plot settings
    h4->GetYaxis()->SetTitle("Ratio");
    h4->GetYaxis()->SetNdivisions(505);
    h4->GetYaxis()->SetTitleSize(20);
    h4->GetYaxis()->SetTitleFont(43);
    h4->GetYaxis()->SetTitleOffset(1.2);
    h4->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
    h4->GetYaxis()->SetLabelSize(15);
    
    // X axis ratio plot settings
    h4->GetXaxis()->SetTitle("M_{JJ} (GeV)");
    
    h4->GetXaxis()->SetTitleSize(20);
    h4->GetXaxis()->SetTitleFont(43);
    h4->GetXaxis()->SetTitleOffset(3.2);
    h4->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
    h4->GetXaxis()->SetLabelSize(15);
}
