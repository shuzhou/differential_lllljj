#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"


//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void geterror() {
    // Define two gaussian histograms. Note the X and Y title are defined
    // at booking time using the convention "Hist_title ; X_title ; Y_title"
    TFile* file = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/background/user.shuzhou.Mc16_ttvv.output_tree_output.root/tree_out.root");
    TTree* t1;
    TTree* t2;
    TTree* t3;
    TTree* t4;
    TTree* t5;
    double error1=0.;
    double error2=0.;
    double error3=0.;
    double error4=0.;
    double weight=0.;
    double s1=0.;
    double s2=0.;
    double s3=0.;
    double s4=0.;
    double s5=0.;
    double sumw=0.;
    file->GetObject("incl",t1);
    file->GetObject("eeee",t2);
    file->GetObject("eemm",t3);
    file->GetObject("mmmm",t4);
    file->GetObject("sumweight",t5);
    //file->GetObject("sumweight",t2);
    t1->SetBranchAddress("inclerror", &error1);
    t2->SetBranchAddress("eeeeerror", &error2);
    t3->SetBranchAddress("eemmerror", &error3);
    t4->SetBranchAddress("mmmmerror", &error4);
    t5->SetBranchAddress("sumweight",&weight);
    for(int i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        if(error1>0){
        s1=sqrt(s1*s1+error1*error1);
        cout<<s1<< " "<<error1<<endl;
        }
    }
    for(int i=0;i<t2->GetEntries();i++){
        
        t2->GetEntry(i);
        if(error2>0){
        s2=sqrt(s2*s2+error2*error2);
        }
    }
    
    for(int i=0;i<t3->GetEntries();i++){
        
        t3->GetEntry(i);
        if(error3>0){
        s3=sqrt(s3*s3+error3*error3);
        }
    }
    for(int i=0;i<t4->GetEntries();i++){
        
        t4->GetEntry(i);
        if(error4>0){
        s4=sqrt(s4*s4+error4*error4);
        }
    }
    for(int i=0;i<t5->GetEntries();i++){
        t5->GetEntry(i);
        sumw=sumw+weight;
    }
    cout<<" incl error is: "<<s1<<endl;
    cout<<" eeee error is: "<<s2<<endl;
    cout<<" eemm error is: "<<s3<<endl;
    cout<<" mmmm error is: "<<s4<<endl;
    cout<<"Total weight is: "<<sumw<<endl;
    
    
    
    
    
    
    
    
    
}