#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"
#include"AtlasStyle.C"
#include"AtlasLabels.C"
#include <iostream>
#include <fstream>
#include <string>
//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void ratioplot1() {
    // Define two gaussian histograms. Note the X and Y title are defined
    // at booking time using the convention "Hist_title ; X_title ; Y_title"
    TFile* file[50];
    ifstream in("tree.txt");
    string s;
    Int_t i=0;
    Int_t j=0;
    Double_t bp[50];
    Double_t gp[50];
    while(i<=39){
        getline(in,s);
    file[i]=TFile::Open(s.c_str());
        cout<<s<<endl;
        i++;
    }
    
    
    SetAtlasStyle();
    TH1F *h1 = new TH1F("h1", "Fake factor;Pt [GeV]; lepton numbers", 50, 0, 50);
    TH1F *h2 = new TH1F("h2", "h2", 50, 0, 50);
    TTree* t1[50];
    TTree* t2[50];
    i=0;
    while(i<=39){
    
    file[i]->GetObject("bePt",t1[i]);
    file[i]->GetObject("gePt",t2[i]);
        t1[i]->SetBranchAddress("bePt", &bp[i]);
        for(j=0;j<t1[i]->GetEntries();j++){
            t1[i]->GetEntry(j);
            h2->Fill(bp[i]);
        }
        t2[i]->SetBranchAddress("gePt", &gp[i]);
        for(j=0;j<t2[i]->GetEntries();j++){
            t2[i]->GetEntry(j);
            h1->Fill(gp[i]);
        }
    


        i++;
    }

   
    
    // Define the Canvas
    TCanvas *c = new TCanvas("c", "canvas", 800, 800);
    
    // Upper plot will be in pad1
    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
    pad1->SetBottomMargin(0); // Upper and lower plot are joined
    pad1->SetGridx();         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();// pad1 becomes the current pad
    h1->SetMinimum(0);  // Define Y ..
    h1->SetMaximum(50000);
    h1->SetStats(0);// No statistics on upper plot
    h1->GetYaxis()->SetTitle("lepton numbers");

    h1->Draw();               // Draw h1
    h2->Draw("same");         // Draw h2 on top of h1
    TLegend *legend = new TLegend(0.4,0.5,0.5,0.6);
    legend->AddEntry(h1,"Bad Lepton","l");
    legend->AddEntry(h2,"Good Lepton","l");
    legend->Draw();
    ATLASLabel(0.6,0.6,"Internal"); 
    // Do not draw the Y axis label on the upper plot and redraw a small
    // axis instead, in order to avoid the first label (0) to be clipped.
    
    
    // lower plot will be in pad
    c->cd();          // Go back to the main canvas before defining pad2
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.3);
    pad2->SetTopMargin(0);
    pad2->SetBottomMargin(0.2);
    pad2->SetGridx(); // vertical grid
    pad2->Draw();
    pad2->cd();       // pad2 becomes the current pad
    
    // Define the ratio plot
    TH1F *h3 = (TH1F*)h1->Clone("h3");
    h3->SetLineColor(kBlack);
    h3->SetMinimum(0);  // Define Y ..
    h3->SetMaximum(0.2); // .. range
    h3->Sumw2();
    h3->SetStats(0);      // No statistics on lower plot
    h3->Divide(h2);
    h3->SetMarkerStyle(21);
    h3->Draw("ep");       // Draw the ratio plot
    
    // h1 settings
    h1->SetLineColor(kBlue+1);
    h1->SetLineWidth(2);
    
    // Y axis h1 plot settings
        // h2 settings
    h2->SetLineColor(kRed);
    h2->SetLineWidth(2);
    
    // Ratio plot (h3) settings
    h3->SetTitle(""); // Remove the ratio title
    
    // Y axis ratio plot settings
    h3->GetYaxis()->SetTitle("fake factor (GL/BL) ");
    h3->GetYaxis()->SetNdivisions(505);
    h3->GetYaxis()->SetTitleSize(20);
    h3->GetYaxis()->SetTitleFont(43);
    h3->GetYaxis()->SetTitleOffset(1.55);
    h3->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
    h3->GetYaxis()->SetLabelSize(15);
    
    // X axis ratio plot settings
    h3->GetXaxis()->SetTitle("Pt [GeV] ");

    h3->GetXaxis()->SetTitleSize(20);
    h3->GetXaxis()->SetTitleFont(43);
    h3->GetXaxis()->SetTitleOffset(4.);
    h3->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
    h3->GetXaxis()->SetLabelSize(15);
}
