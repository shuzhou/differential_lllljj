#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"
#include"THStack.h"
#include "TLatex.h"
#include"AtlasStyle.C"
#include"AtlasLabels.C"
#include "AtlasUtils.C"



//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void plotfakeept() {
    // Define two gaussian histograms. Note the X and Y title are defined
    // at booking time using the convention "Hist_title ; X_title ; Y_title"
    TFile* file1 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/ttbar/fake_2015-2017_ttbar.root");
    TFile* file2 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/ttbar/mc16a/ff_ttbar.root");
    TFile* file3 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/ttbar/mc16a/ff_wt.root");
    TFile* file4 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/f-ttbar-final/ww/user.shuzhou.Mc16_ttbar_ww.output7_tree_output.root/tree_out.root");
    TFile* file5 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/ttbar/mc16a/ff_wz.root");
    TFile* file6 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/ttbar/mc16a/ff_ttw.root");
    TFile* file7= TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/ttbar/mc16a/ff_wtbar.root");
    THStack *hs = new THStack("hs","");
  // SetAtlasStyle();
    gROOT->LoadMacro("AtlasUtils.C");
    
    TH1F *h1 = new TH1F("h1", "Fake factor", 20, 0, 100);
    TH1F *h2 = new TH1F("h2", "", 20, 0, 100);
    TH1F *h3 = new TH1F("h3", "", 20, 0, 100);
    TH1F *h4 = new TH1F("h4", "h4", 20, 0, 100);
   // TH1F *h5 = new TH1F("h5", "h5", 40, 0, 200);
   // TH1F *h6 = new TH1F("h6", "h6", 40, 0, 200);
    TLatex latex;
    TTree* t1;
    TTree* t2;
    TTree* t3;
    TTree* t4;
    TTree* t5;
    TTree* t6;
    TTree* t7;
    TTree* t8;
    double zm1;
    double zm2;
    double zm3;
    double zm4;
    double zm5;
    double zm6;
    double zm7;
    double zm8;
    double w1;
    double w2;
    double w3;
    double w4;
    double w5;
    double w6;
    double w7;
    double w8;
    
    double normal1=0.0004211988;
    double normal2=0.5723;
    double normal3=0.00541407;
    double normal4=0.00799237;
    double normal5=0.01062882;
    double normal6=0.5723;
    file1->GetObject("gePt",t1);
    file1->GetObject("bePt",t2);
    file2->GetObject("gePt",t3);
    file2->GetObject("bePt",t4);
    file3->GetObject("gePt",t5);
    file3->GetObject("bePt",t6);
    file4->GetObject("gePt",t7);
    file4->GetObject("bePt",t8);
    //file5->GetObject("gEta",t5);
    
    t1->SetBranchAddress("gePt", &zm1);
    t2->SetBranchAddress("bePt", &zm2);
    //t2->SetBranchAddress("weight", &w2);
    t3->SetBranchAddress("gePt", &zm3);
    t3->SetBranchAddress("weight", &w3);
    t4->SetBranchAddress("bePt", &zm4);
    t4->SetBranchAddress("weight", &w4);
    t5->SetBranchAddress("gePt", &zm5);
    t5->SetBranchAddress("weight", &w5);
    t6->SetBranchAddress("bePt", &zm6);
    t6->SetBranchAddress("weight", &w6);
    t7->SetBranchAddress("gePt", &zm5);
    t7->SetBranchAddress("weight", &w5);
    t8->SetBranchAddress("bePt", &zm6);
    t8->SetBranchAddress("weight", &w6);
    //t5->SetBranchAddress("gEta", &zm5);
    //t5->SetBranchAddress("weight", &w5);
    for(int i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        h1->Fill(zm1);
    }
    for(int i=0;i<t2->GetEntries();i++){
        t2->GetEntry(i);
        h2->Fill(zm2);
        //h6->Fill(zm2,w2*normal1);
    }
    for(int i=0;i<t3->GetEntries();i++){
        t3->GetEntry(i);
        h3->Fill(zm3,w3*normal1);
        //h6->Fill(zm3,w3*normal2);
    }
    for(int i=0;i<t4->GetEntries();i++){
        t4->GetEntry(i);
        h4->Fill(zm4,w4*normal1);
        //h6->Fill(zm4,w4*normal3);
    }
    for(int i=0;i<t5->GetEntries();i++){
        t5->GetEntry(i);
        h3->Fill(zm5,w5*normal2);
        //h6->Fill(zm3,w3*normal2);
    }
    for(int i=0;i<t6->GetEntries();i++){
        t6->GetEntry(i);
        h4->Fill(zm6,w6*normal2);
        //h6->Fill(zm4,w4*normal3);
    }
    for(int i=0;i<t7->GetEntries();i++){
        t7->GetEntry(i);
        h3->Fill(zm7,w7*normal6);
        //h6->Fill(zm3,w3*normal2);
    }
    for(int i=0;i<t8->GetEntries();i++){
        t6->GetEntry(i);
        h4->Fill(zm8,w8*normal6);
        //h6->Fill(zm4,w4*normal3);
    }
    //for(int i=0;i<t5->GetEntries();i++){
    //    t5->GetEntry(i);
    //    h5->Fill(zm5,w5*normal4);
    //    h6->Fill(zm5,w5*normal4);
   // }
    TH1F *h5 = (TH1F*)h1->Clone("h5");
    TH1F *h6 = (TH1F*)h3->Clone("h6");
    h5->Sumw2();
    h6->Sumw2();
    h4->Sumw2();
    h3->Sumw2();
    h5->Divide(h2);
    h6->Divide(h4);
    h5->SetMarkerStyle(20);
    h6->SetLineColor(kBlue);
    h6->SetLineWidth(2);
    h6->SetFillColor(kBlue);
    // Define the Canvas
    TCanvas *c = new TCanvas("c", "canvas", 600, 600);
    
    // Upper plot will be in pad1
    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
    pad1->SetBottomMargin(0.05); // Upper and lower plot are joined
    // pad1->SetGridx();
    //pad1->SetLogy(1);         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();// pad1 becomes the current pad
    h6->SetMinimum(0);  // Define Y ..
    h6->SetMaximum(1);
    h6->SetStats(0);// No statistics on upper plot
    h6->GetYaxis()->SetTitle("Fake Factor");
    h6->Draw();
    //gStyle->SetErrorY(0.);
    h5->Draw("ep,same");
    //h2->GetYaxis()->SetTitleSize(15);
    //h2->GetYaxis()->SetTitleOffset(0.8);
    
    //h2->Draw("ep");               // Draw h1
    //h5->SetLineColor(kGreen);
   // h5->SetLineWidth(2);
    //h5->SetFillColor(kGreen);
    //hs->Add(h5);
    //h4->SetLineColor(kRed);
    //h4->SetLineWidth(2);
    //h4->SetFillColor(kRed);
    //hs->Add(h4);
    //h3->SetLineColor(kYellow);
    //h3->SetLineWidth(2);
   /// h3->SetFillColor(kYellow);
    //hs->Add(h3);
    //h1->SetLineColor(kBlue);
    //h1->SetLineWidth(2);
    //h1->SetFillColor(kBlue);
   // hs->Add(h1);
    //hs->Draw("same");
    //    // Draw h2 on top of h1
    ATLAS_LABEL(0.4,0.8);
    myText(0.42,0.73,1,"#sqrt{s}=13 TeV   #int Ldt=79.8 fb^{-1}");
    
    TLegend *legend = new TLegend(0.6,0.6,0.85,0.85);
    
    legend->AddEntry(h5,"Data(15-17)","P");
    legend->AddEntry(h6,"MC","F");
    //legend->AddEntry(h3,"wt","F");
    //legend->AddEntry(h4,"wz","F");
    //legend->AddEntry(h5,"ww","F");
    legend->SetBorderSize(0);
    legend->SetFillColor(0);
    legend->Draw();
    
    
    // Do not draw the Y axis label on the upper plot and redraw a small
    // axis instead, in order to avoid the first label (0) to be clipped.
    
    
    // lower plot will be in pad
    c->cd();          // Go back to the main canvas before defining pad2
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.3);
    pad2->SetTopMargin(0);
    pad2->SetBottomMargin(0.26);
    pad2->SetGridx();
    pad2->SetGridy();// vertical grid
    pad2->Draw();
    pad2->cd();       // pad2 becomes the current pad
    
    // Define the ratio plot
    TH1F *h7 = (TH1F*)h5->Clone("h7");
    h7->SetLineColor(kBlack);
    h7->SetMinimum(0);  // Define Y ..
    h7->SetMaximum(3); // .. range
    h7->Sumw2();
    h6->Sumw2();
    h7->SetStats(0);      // No statistics on lower plot
    h7->Divide(h6);
    h7->SetMarkerStyle(2);
    h7->Draw("p");       // Draw the ratio plot
    
    // h1 settings
    
    
    // Y axis h1 plot settings
    // h2 settings
    h2->SetLineColor(kBlack);
    h2->SetMarkerStyle(22);
    
    // Ratio plot (h3) settings
    h7->SetTitle(""); // Remove the ratio title
    
    // Y axis ratio plot settings
    h7->GetYaxis()->SetTitle("Data/Exp");
    h7->GetYaxis()->SetNdivisions(505);
    h7->GetYaxis()->SetTitleSize(20);
    h7->GetYaxis()->SetTitleFont(43);
    h7->GetYaxis()->SetTitleOffset(1.2);
    h7->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
    h7->GetYaxis()->SetLabelSize(15);
    
    // X axis ratio plot settings
    h7->GetXaxis()->SetTitle("p_{T}^{e} (GeV)");
    
    h7->GetXaxis()->SetTitleSize(20);
    h7->GetXaxis()->SetTitleFont(43);
    h7->GetXaxis()->SetTitleOffset(3.2);
    h7->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
    h7->GetXaxis()->SetLabelSize(15);
}
