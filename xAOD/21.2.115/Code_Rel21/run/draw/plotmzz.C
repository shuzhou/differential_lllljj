#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"
#include"THStack.h"
#include "TLatex.h"
#include"AtlasStyle.C"
#include"AtlasLabels.C"
#include "AtlasUtils.C"



//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void plotmzz() {
    // Define two gaussian histograms. Note the X and Y title are defined
    // at booking time using the convention "Hist_title ; X_title ; Y_title"
    TFile* file1 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/minitree-vvloose-ttbar/minitree_2015-2017_vvloose.root");
    TFile* file2 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/minitree-vvloose-ttbar/QCD/tree_out.root");
    TFile* file3 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/minitree-vvloose-ttbar/vbs/tree_out.root");
    TFile* file4 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/minitree-vvloose-ttbar/ggZZ/tree_out.root");
    THStack *hs = new THStack("hs","");
    //THStack *hs = new THStack("hs","");
  // SetAtlasStyle();
    gROOT->LoadMacro("AtlasUtils.C");
    TH1F *h2 = new TH1F("h1", ";MZZ (Gev); Lepton numbers", 14, 0, 700);
    TH1F *h3 = new TH1F("h3", ";MZZ (Gev); Lepton numbers", 14, 0, 700);
    TH1F *h4 = new TH1F("h4", ";MZZ (Gev); Lepton numbers", 14, 0, 700);
    TH1F *h5 = new TH1F("h5", ";MZZ (Gev); Lepton numbers", 14, 0, 700);
    TLatex latex;
    TTree* t1;
    TTree* t2;
    TTree* t3;
    TTree* t4;
    double zm1;
    double zm2;
    double zm3;
    double zm4;
    double zm5;
    double zm6;
    double zm7;
    double zm8;
    file1->GetObject("tree_NOMINAL",t1);
    file2->GetObject("tree_NOMINAL",t2);
    file3->GetObject("tree_NOMINAL",t3);
    file4->GetObject("tree_NOMINAL",t4);
    t1->SetBranchAddress("MZZ", &zm1);
    t1->SetBranchAddress("weight", &zm2);
    t2->SetBranchAddress("MZZ", &zm3);
    t2->SetBranchAddress("weight", &zm4);
    t3->SetBranchAddress("MZZ", &zm5);
    t3->SetBranchAddress("weight", &zm6);
    t4->SetBranchAddress("MZZ", &zm7);
    t4->SetBranchAddress("weight", &zm8);
    h2->Sumw2();
    for(int i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        h2->Fill(zm1,zm2);
      
    }
    
    for(int i=0;i<t2->GetEntries();i++){
        t2->GetEntry(i);
        h3->Fill(zm3,zm4*0.01332897);
    }
    for(int i=0;i<t3->GetEntries();i++){
        t3->GetEntry(i);
        h4->Fill(zm5,zm6*0.0001752);
    }
    for(int i=0;i<t4->GetEntries();i++){
        t4->GetEntry(i);
        h5->Fill(zm7,zm8*0.0403);
    }
    // Define the Canvas
    TCanvas *c = new TCanvas("c", "canvas", 600, 600);
    
    // Upper plot will be in pad1
    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.1, 1, 1.0);
    pad1->SetBottomMargin(0.15); // Upper and lower plot are joined
    // pad1->SetGridx();
   // pad1->SetLogy(1);         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();// pad1 becomes the current pad
    h2->SetMinimum(0);  // Define Y ..
    h2->SetMaximum(0.5);
    h2->Sumw2();
    h2->SetStats(0);// No statistics on upper plot
    h2->GetYaxis()->SetTitle("nEvents/50 Gev ");
    //h2->GetYaxis()->SetTitleSize(15);
    //h2->GetYaxis()->SetTitleOffset(0.8);
    h2->SetMarkerStyle(20);
    h2->Draw("p");// Draw h1
    h3->SetFillColor(kBlue);
    h4->SetFillColor(kRed);
    h5->SetFillColor(kGreen);
    hs->Add(h5);
    hs->Add(h4);
    hs->Add(h3);
    hs->Draw("same");
    
        // Draw h2 on top of h1
    ATLAS_LABEL(0.4,0.8);
    myText(0.42,0.73,1,"#sqrt{s}=13 TeV   #int Ldt=79.91 fb^{-1}");
    TLegend *legend = new TLegend(0.6,0.6,0.85,0.85);
    
    legend->AddEntry(h2,"Data(15-17)","P");
    legend->AddEntry(h3,"ZZ(QCD)","F");
    legend->AddEntry(h4,"ZZ (EWK)","F");
    legend->AddEntry(h5,"ggZZ","F");
    legend->SetBorderSize(0);
    legend->SetFillColor(0);
    legend->Draw();
    
    
    // Do not draw the Y axis label on the upper plot and redraw a small
    // axis instead, in order to avoid the first label (0) to be clipped.
    
    
    // lower plot will be in pad
}
