#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"
#include"THStack.h"
#include<map>
using namespace std;
void systematic(){
    ifstream in1;
    ifstream in2;
    ofstream out1;
    in1.open("/atlas/data18a/shuzhouz/VBSZZ4l/Code_Rel21/source/MyAnalysis/share/tree_name.txt",ios::out);
    out1.open("sys_QCD_16e6050.txt");
    TFile* file = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/minitree-sys-0201/sys_16e.root");
    
    TTree *t1;
    TTree *t2;
    double weight_n,weight_s;
    double sumw=0;
    double err_u=0;
    double err_d=0;
    double sumws=0;
    double mjj;
    double detajj;
    double del;
    double ptj1;
    double ptj2;
    double normal_ewk=0.00017614;
    double normal_QCD=0.0131953;
    double normal_QCD_16e=1252.*140.0/(244.*44300.);
    double normal_QCD_16d=1251.8*43.7/(291.*51690.);
    double err_EG_u=0,err_EG_d=0,err_jet_u=0,err_jet_d=0,err_ele_u=0,err_ele_d=0,err_mu_u=0,err_mu_d=0;
    string name;
    string name1;
    file->GetObject("tree_NOMINAL",t1);
    t1->SetBranchAddress("weight",&weight_n);
    t1->SetBranchAddress("MJJ",&mjj);
    t1->SetBranchAddress("dEtaJJ",&detajj);
    t1->SetBranchAddress("PtJ1",&ptj1);
    t1->SetBranchAddress("PtJ2",&ptj2); 
    for(int i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        if(weight_n==-9999.0) continue;
        if(!((mjj>300)&&(fabs(detajj))>2.0)) continue;
        if(ptj1<60.0||ptj2<50.0) continue;
        sumw=sumw+weight_n;
    }
    sumw=sumw*normal_QCD_16e;
    while(in1>>name){
        sumws=0;
        file->GetObject(name.c_str(),t2);
        t2->SetBranchAddress("weight",&weight_s);
        t2->SetBranchAddress("MJJ",&mjj);
        t2->SetBranchAddress("dEtaJJ",&detajj);
        t2->SetBranchAddress("PtJ1",&ptj1);
        t2->SetBranchAddress("PtJ2",&ptj2);
        for(int j=0;j<t2->GetEntries();j++){
            t2->GetEntry(j);
            if(weight_s==-9999.0) continue;
            
            if(!((mjj>300)&&(fabs(detajj))>2.0)) continue;
           if(ptj1<60.0||ptj2<50.0) continue;
            sumws=sumws+weight_s;
            
        }
        sumws=sumws*normal_QCD_16e;
        del=sumw-sumws;
        name1=name.substr(5);
        cout<<name1<<" "<<sumws<<endl;
        out1<<name1<<" : "<<sumws<<" error: "<<del<<" "<<del/sumw*100<<"%"<<endl;
        if(del<=0){
            err_u=sqrt(err_u*err_u+del*del);
            if(name.find("EG")!=string::npos){
                err_EG_u=sqrt(err_EG_u*err_EG_u+del*del);
            }
            if(name.find("JET")!=string::npos){
                err_jet_u=sqrt(err_jet_u*err_jet_u+del*del);
            }
            if(name.find("EL")!=string::npos){
                err_ele_u=sqrt(err_ele_u*err_ele_u+del*del);
            }
            if(name.find("MUON")!=string::npos){
                err_mu_u=sqrt(err_mu_u*err_mu_u+del*del);
            }
            
        }
        else{
            err_d=sqrt(err_d*err_d+del*del);
            if(name.find("EG")!=string::npos){
                err_EG_d=sqrt(err_EG_d*err_EG_d+del*del);
            }
            if(name.find("JET")!=string::npos){
                err_jet_d=sqrt(err_jet_d*err_jet_d+del*del);
            }
            if(name.find("EL")!=string::npos){
                err_ele_d=sqrt(err_ele_d*err_ele_d+del*del);
            }
            if(name.find("MUON")!=string::npos){
                err_mu_d=sqrt(err_mu_d*err_mu_d+del*del);
            }
        }
    }
    //err_u=sqrt(err_u);
    //err_d=sqrt(err_d);
    cout<<"nominal yeild: "<<sumw<<" sys error up: "<<err_u<<" sys error down: "<<err_d<<endl;
    out1<<"nominal yeild: "<<sumw<<" sys error up: "<<err_u<<" sys error down: "<<err_d<<endl;
    out1<<"EGamma error up: "<<err_EG_u<<" down: "<<err_EG_d<<endl;
    out1<<"Jet error up: "<<err_jet_u<<" down: "<<err_jet_d<<endl;
    out1<<"Electron error up: "<<err_ele_u<<" down: "<<err_ele_d<<endl;
    out1<<"Muon error up: "<<err_mu_u<<" down: "<<err_mu_d<<endl;
    
    
}
