#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"


//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void getweight3() {
   
    TFile* file = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/minitree-trigger/fakebackground/fake_2015-2017_trigger.root");
    TTree* t1;
    double weight;
    double sumw=0;
    double sume=0;
    double nm=1.0;
    double de;
    double mjj;
    double m1;
    double m2;
    
    file->GetObject("tree_NOMINAL",t1);
    //file->GetObject("sumweight",t2);
    t1->SetBranchAddress("weight1", &weight);
    t1->SetBranchAddress("MJJ", &mjj);
    t1->SetBranchAddress("dEtaJJ", &de);
    t1->SetBranchAddress("dEtaJJ", &de);
    t1->SetBranchAddress("MZ1", &m1);
    t1->SetBranchAddress("MZ2", &m2);
    //t1->SetBranchAddress("numfake", &tw);
    for(int i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        if(mjj>300.0&&fabs(de)>2.0){
            
            sumw=sumw+weight;
        sume=sume+weight*weight;
    
        }
    }
    sume=sqrt(sume);
    cout<<"Total event num is: "<<sumw*nm<<" and error is: "<<sume*nm<<endl;
    
    
    
    
}