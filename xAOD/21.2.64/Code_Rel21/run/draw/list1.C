#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"


//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void list1() {
   
    TFile* file = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/minitree-trigger/fakebackground/fake_2015-2017_trigger.root");
    TTree* T1;
   
    int run;
    ULong64_t event;
    double w1;
    double sumw1=0;
    double sume1=0;
    double w2;
    double sumw2=0;
    double sume2=0;
    int t1;
    double sumt1=0;
    int t0;
    double sumt0=0;
    int t2;
    double sumt2=0;
    int t4;
    double sumt4=0;
    int t3;
    double sumt3=0;
    double nm=1.0;
    int quad;
    
    int j=0;
    int k=0;
    
    
    file->GetObject("tree_NOMINAL",T1);
    //file->GetObject("sumweight",t2);
    T1->SetBranchAddress("weight", &w1);
    T1->SetBranchAddress("weight1", &w2);
    T1->SetBranchAddress("trigger0",&t0);
    T1->SetBranchAddress("trigger1",&t1);
    T1->SetBranchAddress("trigger3",&t3);
    T1->SetBranchAddress("trigger2",&t2);
    T1->SetBranchAddress("trigger4",&t4);
    T1->SetBranchAddress("quadtype",&quad);
    //t1->SetBranchAddress("MJ2",&m);
   // t1->SetBranchAddress("dEtaJJ", &de);
    //t1->SetBranchAddress("numfake", &tw);
    for(int i=0;i<T1->GetEntries();i++){
        T1->GetEntry(i);
        //if(quad==2){
        sumw1=sumw1+w1;
        sumw2=sumw2+w2;
        sume1=sume1+w1*w1;
        sume2=sume2+w2*w2;
        sumt0=sumt0+w2*t0;
        sumt1=sumt1+w2*t1;
        sumt2=sumt2+w2*t2;
        sumt3=sumt3+w2*t3;
        sumt4=sumt4+w2*t4;
        //}
        
    }
    sume1=sqrt(sume1);
    sume2=sqrt(sume2);
  
    cout<<"Event num without trigger is: "<<sumw1*nm<<" error is: "<<sume1*nm<<endl;
     cout<<"Event num with trigger is: "<<sumw2*nm<<" error is: "<<sume2*nm<<" ratio is: "<<sumw2/sumw1<<endl;
    cout<<"Event num pass sElectron is: "<<sumt0*nm<<" ratio is: "<<sumt0/sumw1<<endl;
    cout<<"Event num pass mElectron is: "<<sumt1*nm<<" ratio is: "<<sumt1/sumw1<<endl;
    cout<<"Event num pass sMuon is: "<<sumt2*nm<<" ratio is: "<<sumt2/sumw1<<endl;
    cout<<"Event num pass mMuon is: "<<sumt3*nm<<" ratio is: "<<sumt3/sumw1<<endl;
    cout<<"Event num pass E and muon is: "<<sumt4*nm<<" ratio is: "<<sumt4/sumw1<<endl;
    
    
    
    
    
    
    
}