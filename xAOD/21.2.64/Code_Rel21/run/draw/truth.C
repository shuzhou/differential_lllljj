#include "Riostream.h"
#include"TFile.h"
#include"TH1.h"
#include"TPad.h"


//Example displaying two histograms and their ratio.
// Author: Olivier Couet
void truth() {
   
    TFile* file = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/minitree-truth/truth_vbs.root");
    TTree* T1;
   
    double ltype[4];
    double liso[4];
    double ld0[4];
    double lid[4];
    double etaj1,etaj2,detajj,mjj;
    double weight, weight_t;
    
    int ngood;
    int nbad;
    int count=0;
    int count1=0;
    int count2=0;
    ULong64_t event;
    double nm=1.0;
    int nj;
    int run;
    
    
    int j=0;
    int k=0;
    double sumw=0;
    double sumw2=0;
    
    
    file->GetObject("tree_NOMINAL",T1);
    //file->GetObject("sumweight",t2);
    T1->SetBranchAddress("EtaJ1", &etaj1);
    T1->SetBranchAddress("run", &run);
    T1->SetBranchAddress("nJet", &nj);
    T1->SetBranchAddress("event", &event);
    T1->SetBranchAddress("EtaJ2", &etaj2);
    T1->SetBranchAddress("dEtaJJ", &detajj);
    T1->SetBranchAddress("weight", &weight);
    T1->SetBranchAddress("MJJ", &mjj);
    T1->SetBranchAddress("weight_tru", &weight_t);
    
    
    
    
    //t1->SetBranchAddress("numfake", &tw);
    for(int i=0;i<T1->GetEntries();i++){
        T1->GetEntry(i);
        if(weight==-9999.0) continue;
        
        sumw=sumw+weight;
        if(weight_t==-9999.0){
            sumw2=sumw2+weight;
        }
        
        
    }
    cout<<"Total weight: "<<sumw<<endl;
    cout<<"Event without truth is: "<<sumw2<<endl;
    
    
    
    
    
    
    
    
    
}