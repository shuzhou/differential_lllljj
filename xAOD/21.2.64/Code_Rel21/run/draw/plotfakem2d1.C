#include "Riostream.h"
#include"TFile.h"
#include"TH2.h"
#include"TPad.h"
#include"THStack.h"
#include "TLatex.h"
#include"AtlasStyle.C"
#include"AtlasLabels.C"
#include "AtlasUtils.C"




void plotfakem2d1() {
    TFile* file1 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/fake_2015-2017_z+jet.root");
    TFile* file2 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_ttbar.root");
    TFile* file3 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_wt.root");
    TFile* file4 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_wz.root");
    TFile* file5 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_ttw.root");
    TFile* file6 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_wtbar.root");
    TFile* file7 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/f-z+jet/mc16/user.shuzhou.Mc16_ttbar_ww.output46_tree_output.root/tree_out.root");//ww
    TFile* file8 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/f-z+jet/mc16/user.shuzhou.Mc16_QCD.output46_tree_output.root/tree_out.root");
    TFile* file9 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_diboson.root");//dib
    TFile* file10 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_vbs.root");//vbs
    TFile* file11 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_ttz.root");//ttz
    TFile* file12 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/f-z+jet/mc16/user.shuzhou.Mc16_diboson.output47_tree_output.root/tree_out.root");//ggzz
    TFile* file13 = TFile::Open("/lustre/umt3/user/shuzhouz/gridoutput/fake-factor-final/z+jet/mc16/ff_ggZZ.root");//ggzz
    
    THStack *hs = new THStack("hs","");
  // SetAtlasStyle();
    gROOT->LoadMacro("AtlasUtils.C");
    
    TH2F *h1 = new TH2F("h1", "Fake factor (Electron)", 10,0,100,10,-3,3);
    TH2F *h2 = new TH2F("h2", "", 10, 0, 100,10,-3,3);
    TH2F *h4 = new TH2F("h4", "", 10, 0, 100,10,-3,3);
    //TH1F *h3 = new TH1F("h3", "", 20, 0, 100);
    //TH1F *h4 = new TH1F("h4", "h4", 20, 0, 100);
   // TH1F *h5 = new TH1F("h5", "h5", 40, 0, 200);
   // TH1F *h6 = new TH1F("h6", "h6", 40, 0, 200);
    TLatex latex;
    TTree* t1;
    TTree* t2;
    TTree* t3;
    TTree* t4;
    TTree* t5;
    TTree* t6;
    TTree* t7;
    TTree* t8;
    TTree* t9;
    TTree* t10;
    TTree* t11;
    TTree* t12;
    TTree* t13;
    TTree* t14;
    TTree* t15;
    TTree* t16;
    TTree* t17;
    TTree* t18;
    TTree* t19;
    TTree* t20;
    TTree* t21;
    TTree* t22;
    TTree* t23;
    TTree* t24;
    TTree* t25;
    TTree* t26;
    TTree* t27;
    TTree* t28;
    TTree* t29;
    TTree* t30;
    TTree* t31;
    TTree* t32;
    TTree* t33;
    TTree* t34;
    TTree* t35;
    TTree* t36;
    TTree* t37;
    TTree* t38;
    TTree* t39;
    TTree* t40;
    TTree* t41;
    TTree* t42;
    TTree* t43;
    TTree* t44;
    TTree* t45;
    TTree* t46;
    TTree* t47;
    TTree* t48;
    TTree* t49;
    TTree* t50;
    TTree* t51;
    TTree* t52;
    FILE *file14;
    ofstream out;
    file14=fopen("fakem-z+jet-f-1.txt","w+");
    fclose(file14);
    out.open("fakem-z+jet-f-1.txt");
    
    double gept;
    double bept;
    double geeta;
    double beeta;
    double gept1;
    double bept1;
    double geeta1;
    double beeta1;
    double gept2;
    double bept2;
    double geeta2;
    double beeta2;
    double gept3;
    double bept3;
    double geeta3;
    double beeta3;
    double gept4;
    double bept4;
    double geeta4;
    double beeta4;
    double gept5;
    double bept5;
    double geeta5;
    double beeta5;
    double gept6;
    double bept6;
    double geeta6;
    double beeta6;
    double gept7;
    double bept7;
    double geeta7;
    double beeta7;
    double gept8;
    double bept8;
    double geeta8;
    double beeta8;
    double gept9;
    double bept9;
    double geeta9;
    double beeta9;
    double gept10;
    double bept10;
    double geeta10;
    double beeta10;
    double gept11;
    double bept11;
    double geeta11;
    double beeta11;
    double gept12;
    double bept12;
    double geeta12;
    double beeta12;
    double zm5;
    double zm6;
    double zm7;
    double zm8;
    double w1;
    double w2;
    double w3;
    double w4;
    double w5;
    double w6;
    double w7;
    double w8;
    double w9,w10,w11,w12,w13,w14,w15,w16,w17,w18,w19,w20,w21,w22,w23,w24;
    double z;
    double x1;
    double y1;
    double x2,y2,bin;
    
    double normal1=0.00042097;  //ttbar
    double normal2=0.5723; //wt
    double normal3=0.00541407;//ww
    double normal4=0.00799237;//wz
    double normal5=0.01062882;//ttw
    double normal6=0.5723;//wtbar
    double normal7=0.013329; //QCD
    double normal8=0.0001752;//EWK VBS
    double normal9=0.0403105;//ggZZ
    double normal10=0.03344;//diboson
    double normal11=0.0266; //ttz
    double normal12=0.0689;      //diboson1
    
    
    
    file1->GetObject("gmPt",t1);
    file1->GetObject("bmPt1",t2);
    file1->GetObject("gmEta",t3);
    file1->GetObject("bmEta1",t4);
    file2->GetObject("gmPt",t5);
    file2->GetObject("bmPt1",t6);
    file2->GetObject("gmEta",t7);
    file2->GetObject("bmEta1",t8);
    file3->GetObject("gmPt",t9);
    file3->GetObject("bmPt1",t10);
    file3->GetObject("gmEta",t11);
    file3->GetObject("bmEta1",t12);
    file4->GetObject("gmPt",t13);
    file4->GetObject("bmPt1",t14);
    file4->GetObject("gmEta",t15);
    file4->GetObject("bmEta1",t16);
    file5->GetObject("gmPt",t17);
    file5->GetObject("bmPt1",t18);
    file5->GetObject("gmEta",t19);
    file5->GetObject("bmEta1",t20);
    file6->GetObject("gmPt",t21);
    file6->GetObject("bmPt1",t22);
    file6->GetObject("gmEta",t23);
    file6->GetObject("bmEta1",t24);
    file7->GetObject("gmPt",t25);
    file7->GetObject("bmPt1",t26);
    file7->GetObject("gmEta",t27);
    file7->GetObject("bmEta1",t28);
    file8->GetObject("gmPt",t29);
    file8->GetObject("bmPt1",t30);
    file8->GetObject("gmEta",t31);
    file8->GetObject("bmEta1",t32);
    file9->GetObject("gmPt",t33);
    file9->GetObject("bmPt1",t34);
    file9->GetObject("gmEta",t35);
    file9->GetObject("bmEta1",t36);
    file10->GetObject("gmPt",t37);
    file10->GetObject("bmPt1",t38);
    file10->GetObject("gmEta",t39);
    file10->GetObject("bmEta1",t40);
    file11->GetObject("gmPt",t41);
    file11->GetObject("bmPt1",t42);
    file11->GetObject("gmEta",t43);
    file11->GetObject("bmEta1",t44);
    file12->GetObject("gmPt",t45);
    file12->GetObject("bmPt1",t46);
    file12->GetObject("gmEta",t47);
    file12->GetObject("bmEta1",t48);
    file13->GetObject("gmPt",t49);
    file13->GetObject("bmPt1",t50);
    file13->GetObject("gmEta",t51);
    file13->GetObject("bmEta1",t52);

    
    
    
    //file5->GetObject("gEta",t5);
    
    t1->SetBranchAddress("gmPt", &gept);
    t2->SetBranchAddress("bmPt1", &bept);
    //t2->SetBranchAddress("weight", &w2);
    t3->SetBranchAddress("gmEta", &geeta);
    t4->SetBranchAddress("bmEta1", &beeta);
    t5->SetBranchAddress("gmPt", &gept1);
    t6->SetBranchAddress("bmPt1", &bept1);
    t5->SetBranchAddress("weight", &w1);
    t6->SetBranchAddress("weight", &w2);
    t7->SetBranchAddress("gmEta", &geeta1);
    t8->SetBranchAddress("bmEta1", &beeta1);
    t9->SetBranchAddress("gmPt", &gept2);
    t10->SetBranchAddress("bmPt1", &bept2);
    t9->SetBranchAddress("weight", &w3);
    t10->SetBranchAddress("weight", &w4);
    t11->SetBranchAddress("gmEta", &geeta2);
    t12->SetBranchAddress("bmEta1", &beeta2);
    t13->SetBranchAddress("gmPt", &gept3);
    t13->SetBranchAddress("weight", &w5);
    t14->SetBranchAddress("bmPt1", &bept3);
    t14->SetBranchAddress("weight", &w6);
    t15->SetBranchAddress("gmEta", &geeta3);
    t16->SetBranchAddress("bmEta1", &beeta3);
    t17->SetBranchAddress("gmPt", &gept4);
    t17->SetBranchAddress("weight", &w7);
    t18->SetBranchAddress("bmPt1", &bept4);
    t18->SetBranchAddress("weight", &w8);
    t19->SetBranchAddress("gmEta", &geeta4);
    t20->SetBranchAddress("bmEta1", &beeta4);
    t21->SetBranchAddress("gmPt", &gept5);
    t21->SetBranchAddress("weight", &w9);
    t22->SetBranchAddress("bmPt1", &bept5);
    t22->SetBranchAddress("weight", &w10);
    t23->SetBranchAddress("gmEta", &geeta5);
    t24->SetBranchAddress("bmEta1", &beeta5);
    t25->SetBranchAddress("gmPt", &gept6);
    t25->SetBranchAddress("weight", &w11);
    t26->SetBranchAddress("bmPt1", &bept6);
    t26->SetBranchAddress("weight", &w12);
    t27->SetBranchAddress("gmEta", &geeta6);
    t28->SetBranchAddress("bmEta1", &beeta6);
    t29->SetBranchAddress("gmPt", &gept7);
    t29->SetBranchAddress("weight", &w13);
    t30->SetBranchAddress("bmPt1", &bept7);
    t30->SetBranchAddress("weight", &w14);
    t31->SetBranchAddress("gmEta", &geeta7);
    t32->SetBranchAddress("bmEta1", &beeta7);
    t33->SetBranchAddress("gmPt", &gept8);
    t33->SetBranchAddress("weight", &w15);
    t34->SetBranchAddress("bmPt1", &bept8);
    t34->SetBranchAddress("weight", &w16);
    t35->SetBranchAddress("gmEta", &geeta8);
    t36->SetBranchAddress("bmEta1", &beeta8);
    t37->SetBranchAddress("gmPt", &gept9);
    t37->SetBranchAddress("weight", &w17);
    t38->SetBranchAddress("bmPt1", &bept9);
    t38->SetBranchAddress("weight", &w18);
    t39->SetBranchAddress("gmEta", &geeta9);
    t40->SetBranchAddress("bmEta1", &beeta9);
    t41->SetBranchAddress("gmPt", &gept10);
    t41->SetBranchAddress("weight", &w19);
    t42->SetBranchAddress("bmPt1", &bept10);
    t42->SetBranchAddress("weight", &w20);
    t43->SetBranchAddress("gmEta", &geeta10);
    t44->SetBranchAddress("bmEta1", &beeta10);
    t45->SetBranchAddress("gmPt", &gept11);
    t45->SetBranchAddress("weight", &w21);
    t46->SetBranchAddress("bmPt1", &bept11);
    t46->SetBranchAddress("weight", &w22);
    t47->SetBranchAddress("gmEta", &geeta11);
    t48->SetBranchAddress("bmEta1", &beeta11);
    t49->SetBranchAddress("gmPt", &gept12);
    t49->SetBranchAddress("weight", &w23);
    t50->SetBranchAddress("bmPt1", &bept12);
    t50->SetBranchAddress("weight", &w24);
    t51->SetBranchAddress("gmEta", &geeta12);
    t52->SetBranchAddress("bmEta1", &beeta12);
    
    
    //t5->SetBranchAddress("gEta", &zm5);
    //t5->SetBranchAddress("weight", &w5);
    for(int i=0;i<t1->GetEntries();i++){ //data
            t1->GetEntry(i);
            t3->GetEntry(i);
        h1->Fill(gept,geeta);
            cout<<i<<endl;
        //}
    }
    for(int i=0;i<t2->GetEntries();i++){//data
                t2->GetEntry(i);
            t4->GetEntry(i);
        h2->Fill(bept,beeta);
            cout<<i<<endl;
            }
    for(int i=0;i<t5->GetEntries();i++){ //ttbar
                t5->GetEntry(i);
        t7->GetEntry(i);
        h1->Fill(gept1,geeta1,-w1*normal1*1);
        cout<<i<<endl;
       
    }
    for(int i=0;i<t6->GetEntries();i++){ //ttbar
                t6->GetEntry(i);
        t8->GetEntry(i);
        h2->Fill(bept,beeta,-w2*normal1*1);
        cout<<i<<endl;
           }
    for(int i=0;i<t9->GetEntries();i++){ //wt
        t9->GetEntry(i);
        t11->GetEntry(i);
        h1->Fill(gept2,geeta2,-w3*normal2*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t10->GetEntries();i++){  //wt
        t10->GetEntry(i);
        t12->GetEntry(i);
        h2->Fill(bept2,beeta2,-w4*normal2*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t13->GetEntries();i++){ //wz
        t13->GetEntry(i);
        t15->GetEntry(i);
        h1->Fill(gept3,geeta3,-w5*normal4*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t14->GetEntries();i++){  //wz
        t14->GetEntry(i);
        t16->GetEntry(i);
        h2->Fill(bept3,beeta3,-w6*normal4*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t17->GetEntries();i++){ //ttw
        t17->GetEntry(i);
        t19->GetEntry(i);
        h1->Fill(gept4,geeta4,-w7*normal5*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t18->GetEntries();i++){  //ttw
        t18->GetEntry(i);
        t20->GetEntry(i);
        h2->Fill(bept4,beeta4,-w8*normal5*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t21->GetEntries();i++){ //wtbar
        t21->GetEntry(i);
        t23->GetEntry(i);
        h1->Fill(gept5,geeta5,-w9*normal6*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t22->GetEntries();i++){  //wtbar
        t22->GetEntry(i);
        t24->GetEntry(i);
        h2->Fill(bept5,beeta5,-w10*normal6*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t25->GetEntries();i++){ //ww
        t25->GetEntry(i);
        t27->GetEntry(i);
        h1->Fill(gept6,geeta6,-w11*normal3*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t26->GetEntries();i++){  //ww
        t26->GetEntry(i);
        t28->GetEntry(i);
        h2->Fill(bept6,beeta6,-w12*normal3*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t29->GetEntries();i++){ //QCD
        t29->GetEntry(i);
        t31->GetEntry(i);
        h1->Fill(gept7,geeta7,-w13*normal7*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t30->GetEntries();i++){  //QCD
        t30->GetEntry(i);
        t32->GetEntry(i);
        h2->Fill(bept7,beeta7,-w14*normal7*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t33->GetEntries();i++){ //diboson
        t33->GetEntry(i);
        t35->GetEntry(i);
        h1->Fill(gept8,geeta8,-w15*normal10*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t34->GetEntries();i++){  //diboson
        t34->GetEntry(i);
        t36->GetEntry(i);
        h2->Fill(bept8,beeta8,-w16*normal10*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t37->GetEntries();i++){ //vbs
        t37->GetEntry(i);
        t39->GetEntry(i);
        h1->Fill(gept9,geeta9,-w17*normal8*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t38->GetEntries();i++){  //vbs
        t38->GetEntry(i);
        t40->GetEntry(i);
        h2->Fill(bept9,beeta9,-w18*normal8*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t41->GetEntries();i++){ //ttz
        t41->GetEntry(i);
        t43->GetEntry(i);
        h1->Fill(gept10,geeta10,-w19*normal11*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t42->GetEntries();i++){  //ttz
        t42->GetEntry(i);
        t44->GetEntry(i);
        h2->Fill(bept10,beeta10,-w20*normal11*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t45->GetEntries();i++){ //dib1
        t45->GetEntry(i);
        t47->GetEntry(i);
        h1->Fill(gept11,geeta11,-w21*normal12*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t46->GetEntries();i++){  //dib1
        t46->GetEntry(i);
        t48->GetEntry(i);
        h2->Fill(bept11,beeta11,-w22*normal12*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t49->GetEntries();i++){ //ggzz
        t49->GetEntry(i);
        t51->GetEntry(i);
        h1->Fill(gept12,geeta12,-w23*normal9*1);
        cout<<i<<endl;
    }
    for(int i=0;i<t50->GetEntries();i++){  //ggzz
        t50->GetEntry(i);
        t52->GetEntry(i);
        h2->Fill(bept12,beeta12,-w24*normal9*1);
        cout<<i<<endl;
    }
    TH1F *h3 = (TH1F*)h1->Clone("h3");
    //TH1F *h6 = (TH1F*)h3->Clone("h6");
    //h5->Sumw2();
    //h6->Sumw2();
   // h4->Sumw2();
    h3->Sumw2();
    h3->Divide(h2);
    //h6->Divide(h4);
    //h5->SetMarkerStyle(20);
    //h6->SetLineColor(kBlue);
    //h6->SetLineWidth(2);
    //h6->SetFillColor(kBlue);
    // Define the Canvas
    TCanvas *c = new TCanvas("c", "canvas", 600, 600);
    
    // Upper plot will be in pad1
    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.1, 0.9, 1.0);
    pad1->SetBottomMargin(0.1); // Upper and lower plot are joined
    // pad1->SetGridx();
    //pad1->SetLogy(1);         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();// pad1 becomes the current pad
    //h6->SetMinimum(0);  // Define Y ..
    //h6->SetMaximum(0.2);
    for(int i=1;i<11;i++){
        for(int j=1;j<11;j++){
            x2=5.0+(i-1)*10.0;
            y2=-2.7+(j-1)*0.6;
            bin=h3->GetBinContent(i,j);
            if(bin<0){
                bin=0;
            }
            h4->Fill(x2,y2,bin);
            
        }
    }
    h4->SetStats(0);// No statistics on upper plot
    h4->GetYaxis()->SetTitle("#eta_{#mu}");
    h4->GetXaxis()->SetTitle("P_{T}^{#mu} (GeV)");
    h4->GetZaxis()->SetRangeUser(-0.1,1);
    //h3->GetYaxis()->SetTitleSize(8);
    //h3->GetXaxis()->SetTitleSize(15);
    h4->Draw("colz");
    //gStyle->SetErrorY(0.);
    //h5->Draw("ep,same");
    //h3->GetYaxis()->SetTitleSize(8);
    //h3->GetXaxis()->SetTitleSize(15);
    
    //h3->GetYaxis()->SetTitleOffset(0.8);
    
    //h2->Draw("ep");               // Draw h1
    //h5->SetLineColor(kGreen);
   // h5->SetLineWidth(2);
    //h5->SetFillColor(kGreen);
    //hs->Add(h5);
    //h4->SetLineColor(kRed);
    //h4->SetLineWidth(2);
    //h4->SetFillColor(kRed);
    //hs->Add(h4);
    //h3->SetLineColor(kYellow);
    //h3->SetLineWidth(2);
   /// h3->SetFillColor(kYellow);
    //hs->Add(h3);
    //h1->SetLineColor(kBlue);
    //h1->SetLineWidth(2);
    //h1->SetFillColor(kBlue);
   // hs->Add(h1);
    //hs->Draw("same");
    //    // Draw h2 on top of h1
    ATLAS_LABEL(0.4,0.8);
    myText(0.42,0.73,1,"#sqrt{s}=13 TeV   #int Ldt=79.91 fb^{-1}");
    
   // TLegend *legend = new TLegend(0.6,0.6,0.85,0.85);
    
   // legend->AddEntry(h5,"Data(15-17) 1jet","P");
    //legend->AddEntry(h6,"Data 2jet","F");
   // //legend->AddEntry(h3,"wt","F");
    //legend->AddEntry(h4,"wz","F");
    //legend->AddEntry(h5,"ww","F");
    //legend->SetBorderSize(0);
    //legend->SetFillColor(0);
    //legend->Draw();
    
    
    // Do not draw the Y axis label on the upper plot and redraw a small
    // axis instead, in order to avoid the first label (0) to be clipped.
    
    
    // lower plot will be in pad
   // c->cd();          // Go back to the main canvas before defining pad2
    //TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.3);
    for(int i=1;i<11;i++){
        x1=5.0+i*10.0;
        for(int j=1;j<11;j++){
            y1=-0.3+0.6*j;
            z=h4->GetBinContent(i,j);
            out<<z<<endl;
            
        }
    }
   }
