################################################################################
# Package: MyAnalysis
################################################################################

SET(Boost_NO_BOOST_CMAKE ON CACHE BOOL "TurnOff BoostFinder" FORCE)

# Declare the package name:
atlas_subdir( MyAnalysis )

# Define a precompiler variable for the release. Rel 20.1 == 2001; 20.7 = 2007; 21.0 - 2100
add_definitions( -D__RELEASE__=2100 )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
	PhysicsAnalysis/D3PDTools/EventLoopGrid
	PhysicsAnalysis/D3PDTools/EventLoop
	PhysicsAnalysis/D3PDTools/EventLoopAlgs
	Event/xAOD/xAODEventInfo
	Event/xAOD/xAODJet
	Control/xAODRootAccess
	Event/xAOD/xAODMuon
	Event/xAOD/xAODTracking
	Control/AthLinks
	Event/xAOD/xAODCaloEvent
	Control/AthContainers
	Event/xAOD/xAODEgamma
	Event/xAOD/xAODMissingET
	PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections
	Event/xAOD/xAODTruth
	PhysicsAnalysis/MuonID/MuonSelectorTools
	PhysicsAnalysis/AnalysisCommon/FsrUtils
	Trigger/TrigAnalysis/TrigDecisionTool
	Event/xAOD/xAODTrigMuon
	Trigger/TrigConfiguration/TrigConfxAOD
	PhysicsAnalysis/AnalysisCommon/IsolationSelection
	DataQuality/GoodRunsLists
	PhysicsAnalysis/AnalysisCommon/PileupReweighting
	Event/xAOD/xAODBTagging
	AssociationUtils/AssociationUtils
	Event/xAOD/xAODCutFlow
	Event/xAOD/xAODCore
	Tools/PathResolver
        #IsolationTool/IsolationTool
        #RecoToolInterfaces/RecoToolInterfaces
        Reconstruction/MET/METUtilities
        PhysicsAnalysis/HiggsPhys/Run2/HZZ/Tools/ZMassConstraint
        PhysicsAnalysis/TopPhys/QuickAna
        #QuickAna/QuickAna
)


# Find the needed external(s):
find_package( ROOT COMPONENTS Core RIO Hist Tree )

# build a CINT dictionary for the library
atlas_add_root_dictionary ( MyAnalysisLib MyAnalysisCintDict
	ROOT_HEADERS MyAnalysis/MyxAODAnalysis.h Root/LinkDef.h
                            EXTERNAL_PACKAGES ROOT
)

# build a shared library
atlas_add_library( MyAnalysisLib
	MyAnalysis/*.h Root/*  ${MyAnalysisCintDict}
                   PUBLIC_HEADERS MyAnalysis
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES}
                   EventLoop
                   EventLoopAlgs
                   EventLoopGrid
                   GoodRunsListsLib
                   IsolationSelectionLib
                   MuonEfficiencyCorrectionsLib
                   MuonMomentumCorrectionsLib
                   MuonSelectorToolsLib
                   PathResolver
                   PileupReweightingLib
                   TrigConfxAODLib
                   TrigDecisionToolLib
                   xAODBTagging
                   xAODEgamma
                   xAODEventInfo
                   xAODJet
                   xAODMissingET
                   xAODMuon
                   xAODRootAccess
                   xAODTracking
                   xAODTrigEgamma
                   xAODTruth
		   xAODCutFlow
		   FsrUtilsLib
		   QuickAnaLib
		   ElectronPhotonSelectorToolsLib
		   ElectronPhotonFourMomentumCorrectionLib
		   TriggerMatchingToolLib
                   METUtilitiesLib
                   #RecoToolInterfacesLib
                   AssociationUtilsLib
                   ZMassConstraintLib
)

if( XAOD_STANDALONE )
   foreach( exec runFourLep grid_mc16a_EWK grid_mc16a_triboson grid_mc16a_diboson grid_mc16a_ttvv grid_mc16a_QCD grid_mc16a_ggZZ grid_mc16a_vbs grid_mc16a_wtbar grid_mc16a_DYwmm grid_mc16a_ttz grid_mc16a_zee grid_mc16a_zmumu grid_data15_periodD grid_data15_periodE grid_data15_periodF grid_data15_periodG grid_data15_periodH grid_data15_periodJ grid_data16_periodA grid_data16_periodB grid_data16_periodC grid_data16_periodD grid_data16_periodE grid_data16_periodF grid_data16_periodG grid_data16_periodI grid_data16_periodK grid_data16_periodL grid_mc16a_zzllvv grid_data17_rerun grid_mc16a_ttw grid_data17_periodA grid_data17_periodB
           grid_data18_all grid_data17_periodC grid_data17_periodD grid_mc16a_zee grid_mc16a_wt grid_mc16a_ztau grid_mc16a_ttbar grid_mc16a_4l grid_mc16a_zzllll grid_mc15_ttbar grid_mc16a_ttbar1 grid_data16_demo grid_data17_demo grid_data16_all grid_data17_all grid_data17_periodE grid_data17_periodF grid_data17_periodH grid_data17_periodI grid_data17_periodK grid_data15_all)
      atlas_add_executable( ${exec}
         util/${exec}.cxx
         INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
         LINK_LIBRARIES ${ROOT_LIBRARIES} ${Boost_LIBRARIES}
         MyAnalysisLib)
   endforeach()
endif()

# Install files from the package:
#atlas_install_scripts( scripts/*.py )
atlas_install_data( share/* )
