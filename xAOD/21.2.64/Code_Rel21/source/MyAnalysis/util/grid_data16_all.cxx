#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"

#include <PATInterfaces/SystematicCode.h>
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/SystematicsUtil.h"

#include "MyAnalysis/MyxAODAnalysis.h"
#include "PathResolver/PathResolver.h"

#include <iostream>     // std::cout
#include <fstream>

using namespace std;

int main( int argc, char* argv[] ) {
    
    // Take the submit directory from the input if provided:
    string varfile1 = PathResolverFindCalibFile("MyAnalysis/data16.txt");
    ifstream in1;
    in1.open(varfile1.c_str(),ios::out);
    string input_name;
    string output_name;
    string datasetnum;
    int pos;
    
    while(in1>>input_name){
        system("rm -rf user*");
        
        std::string submitDir = "user.shuzhou.data16_output1";
        if( argc > 1 ) submitDir = argv[ 1 ];
        
        // Set up the job for xAOD access:
        xAOD::Init().ignore();
        
        // Construct the samples to run on:
        SH::SampleHandler sh;
        pos=input_name.find(".",13);
        datasetnum=input_name.substr(13,pos-13);
        output_name="user.shuzhou.data16_"+datasetnum+"_vbszz_0121.output";
        
        
        
        SH::scanRucio (sh, input_name.c_str());
        
        // Set the name of the input TTree. It's always "CollectionTree"
        // for xAOD files.
        sh.setMetaString( "nc_tree", "CollectionTree" );
        
        // Print what we found:
        sh.print();
        
        // Create an EventLoop job:
        EL::Job job;
        job.sampleHandler( sh );
        
        // Add our analysis to the job:
        MyxAODAnalysis* alg = new MyxAODAnalysis("physics");
        
        job.algsAdd( alg );
        
        // Run the job using the local/direct driver:
        //EL::DirectDriver driver;
        EL::PrunDriver driver;
        driver.options()->setString("nc_outputSampleName", output_name.c_str());
        //driver.options()->setString(EL::Job::optGridNFilesPerJob, "MAX"); //By default, split in as few jobs as possible
        driver.options()->setDouble("nc_nFilesPerJob", 10);
        driver.options()->setDouble("nc_mergeOutput", 0);
        //sh.get("mc14_13TeV.110401.PowhegPythia_P2012_ttbar_nonallhad.merge.DAOD_STDM4.e2928_s1982_s2008_r5787_r5853_p1807/")->SetMetaDouble(EL::Job::optGridNFilesPerJob, 1); //For this particular sample, split into one job per input file
        //driver.options()->setDouble(EL::Job::optGridMergeOutput, 1); //run merging jobs for all samples before downloading (recommended)
        //driver.submit( job, submitDir );
        driver.submitOnly (job, submitDir);
    }
    
    
    return 0;
}
